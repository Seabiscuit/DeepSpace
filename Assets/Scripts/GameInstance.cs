﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class GameInstance : MonoBehaviour
{
	public static Assembly gameAssembly = typeof(GameInstance).Assembly;
	public static GameInstance singleton;
	private static uint lastIdAllocated;
	private bool isServer;

	public GameObject GetPrefab(string filepath)
	{
		GameObject prefab;
		if (AssetManager.singleton.TryGetAssetWithoutLoad<GameObject>(filepath, out prefab))
			return prefab;
		return AssetManager.singleton.LoadPrefab(filepath);
	}

	private GameObject InstantiatePrefab(string filepath, Vector3? position = null, Quaternion? rotation = null)
	{
		if (string.IsNullOrEmpty(filepath))
			return null;
		GameObject prefab = this.GetPrefab(filepath);
		if (prefab == null)
		{
			ClientConsole.singleton.Log("Unable to find prefab " + filepath);
			return null;
		}
		return MonoBehaviour.Instantiate(prefab, position.HasValue ? position.Value : prefab.transform.position, rotation.HasValue ? rotation.Value : prefab.transform.rotation);
	}

	private GameObject InstantiatePrefab(GameObject prefab, Vector3? position = null, Quaternion? rotation = null)
	{
		if (prefab == null)
		{
			ClientConsole.singleton.Log("Trying to instantiate a null prefab");
			return null;
		}
		return MonoBehaviour.Instantiate(prefab, position.HasValue ? position.Value : prefab.transform.position, rotation.HasValue ? rotation.Value : prefab.transform.rotation);
	}

	public GameObject SpawnPrefab(string filePath)
	{
		GameObject obj = this.InstantiatePrefab(filePath, null, null);
		if (obj != null)
			obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(string filePath, Transform parent)
	{
		GameObject obj = this.InstantiatePrefab(filePath, null, null);
		if (obj == null)
			return null;
		obj.transform.SetParent(parent, false);
		obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(string filePath, Vector3 position, Quaternion rotation)
	{
		GameObject obj = this.InstantiatePrefab(filePath, position, rotation);
		if (obj != null)
			obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(string filePath, Transform parent, Vector3 localPosition, Quaternion localRotation)
	{
		GameObject obj = this.InstantiatePrefab(filePath, null, null);
		if (obj == null)
			return null;
		obj.transform.SetParent(parent, false);
		obj.transform.localPosition = localPosition;
		obj.transform.localRotation = localRotation;
		obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(GameObject prefab)
	{
		GameObject obj = this.InstantiatePrefab(prefab, null, null);
		if (obj != null)
			obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(GameObject prefab, Transform parent)
	{
		GameObject obj = this.InstantiatePrefab(prefab, null, null);
		if (obj == null)
			return null;
		obj.transform.SetParent(parent, false);
		obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(GameObject prefab, Vector3 position, Quaternion rotation)
	{
		GameObject obj = this.InstantiatePrefab(prefab, position, rotation);
		if (obj != null)
			obj.SetActive(true);
		return obj;
	}

	public GameObject SpawnPrefab(GameObject prefab, Transform parent, Vector3 localPosition, Quaternion localRotation)
	{
		GameObject obj = this.InstantiatePrefab(prefab, null, null);
		if (obj == null)
			return null;
		obj.transform.SetParent(parent, false);
		obj.transform.localPosition = localPosition;
		obj.transform.localRotation = localRotation;
		obj.SetActive(true);
		return obj;
	}

	public BaseEntity CreateEntity(string prefab)
	{
		GameObject obj = this.SpawnPrefab(prefab);
		if (obj == null)
			return null;
		BaseEntity entity = obj.GetComponent<BaseEntity>();
		if (entity == null)
			Debug.LogError("Tried to create an entity from a prefab that isn't an entity");
		entity.peer = Peer.singleton;
		entity.prefabName = prefab;
		if (isServer)
			entity.NetworkInit();
		return entity;
	}

	public BaseEntity CreateEntity(string prefab, Vector3 position, Quaternion rotation)
	{
		GameObject obj = this.SpawnPrefab(prefab, position, rotation);
		if (obj == null)
			return null;
		BaseEntity entity = obj.GetComponent<BaseEntity>();
		if (entity == null)
			Debug.LogError("Tried to create an entity from a prefab that isn't an entity");
		entity.OnTargetTransformUpdate(position, rotation);
		entity.peer = Peer.singleton;
		entity.prefabName = prefab;
		if (isServer)
			entity.NetworkInit();
		return entity;
	}

	public void DestroyObject(GameObject obj, float delay = 0.0f)
	{
		if (obj != null)
			MonoBehaviour.Destroy(obj, delay);
		else
			Debug.LogError("Tried to destroy an object that was null");
	}

	public void DestroyEntity(GameObject obj, float delay = 0.0f)
	{
		if (obj == null)
			return;
		BaseEntity entity = obj.GetComponent<BaseEntity>();
		if (entity == null)
			Debug.LogError("Tried to destroy an object that isn't an entity");
		if (isServer && entity.IsNetworked())
			entity.NetworkDestroy();
		else
			entity.LocalDestroy();
		entity.peer = null;
		this.DestroyObject(obj, delay);
	}

	private void OnEnable()
	{
		Physics.autoSimulation = false;
		AssetManager.singleton.LoadAssetBundle("gamebundles/export");
		AssetManager.singleton.prefabs = this.transform;
		AssetManager.singleton.LoadAllPrefabs();
		//ClientConsole.singleton.Log("Loaded " + this.prefabsByFilepath.Count + " prefabs");
		this.DrawDebugButtons();
		singleton = this;
	}

	public uint AllocateId()
	{
		Debug.Assert(GameInstance.lastIdAllocated < uint.MaxValue, "Id allocation exceeded maximum");
		uint id = ++GameInstance.lastIdAllocated;
		Debug.Assert(this.isServer, "Id allocation should be done serverside");
		return id;
	}

	private void DrawDebugButtons()
	{
		Transform parent = MonoBehaviour.FindObjectOfType<Canvas>().transform;
		//GameObject button1 = this.SpawnPrefab("assets/prefabs/native/button.prefab");
		GameObject prefab = this.GetPrefab("assets/prefabs/native/button.prefab");
		if (prefab == null)
			Debug.LogError("Debug button prefab not found");

		GameObject button1 = this.SpawnPrefab(prefab, parent);
		button1.GetComponent<RectTransform>().localPosition = Vector3.left * 100.0f;
		button1.GetComponentInChildren<Text>().text = "Server";

		GameObject button2 = this.SpawnPrefab(prefab, parent);
		button2.GetComponent<RectTransform>().localPosition = Vector3.right * 100.0f;
		button2.GetComponentInChildren<Text>().text = "Client";

		button1.GetComponent<Button>().onClick.AddListener(() =>
		{
			this.gameObject.AddComponent<GameServer>();
			GameObject.Destroy(button1);
			GameObject.Destroy(button2);
			this.isServer = true;
		});

		button2.GetComponent<Button>().onClick.AddListener(() =>
		{
			this.gameObject.AddComponent<GameClient>();
			GameObject.Destroy(button1);
			GameObject.Destroy(button2);
			this.isServer = false;
		});
	}
}