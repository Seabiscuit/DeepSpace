﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionPoint : MonoBehaviour
{
	public Vector3 offset = new Vector3(0.0f, 0.0f, 0.0f);
	public bool isMale;
	[Header("Must be filled out to work!")]
	public string debugName;
	[NonSerialized]
	public BaseBuildable owner;
	[NonSerialized]
	public uint ownerId;
	[NonSerialized]
	public uint constructionId;

	private void Awake()
	{
		this.owner = this.GetComponentInParent<BaseBuildable>();
		this.ownerId = this.owner.networkableId;
	}

	public Vector3 GetCenter()
	{
		return this.transform.position + this.transform.rotation * this.offset;
	}

	public Vector3 GetPlacementPosition(Bounds bounds, Quaternion rotation)
	{
		return this.GetCenter() + rotation * bounds.extents;
	}
}