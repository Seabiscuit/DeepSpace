﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class InputMappings
{
	public static readonly Dictionary<InputBinding, KeyCode> keyMappings = new Dictionary<InputBinding, KeyCode>(32);
	private static string keymapPath = "keymap.config";

	public static void SaveKeymap(bool overwrite = true)
	{
	}

	public static void LoadKeymap(bool overwrite = true)
	{
		InputMappings.keyMappings.Clear();
		InputMappings.keyMappings.Add(InputBinding.Forward, KeyCode.W);
		InputMappings.keyMappings.Add(InputBinding.Backward, KeyCode.S);
		InputMappings.keyMappings.Add(InputBinding.StrafeRight, KeyCode.D);
		InputMappings.keyMappings.Add(InputBinding.StrafeLeft, KeyCode.A);
		InputMappings.keyMappings.Add(InputBinding.Up, KeyCode.Space);
		InputMappings.keyMappings.Add(InputBinding.Down, KeyCode.LeftControl);
		InputMappings.keyMappings.Add(InputBinding.RollRight, KeyCode.E);
		InputMappings.keyMappings.Add(InputBinding.RollLeft, KeyCode.Q);
		InputMappings.keyMappings.Add(InputBinding.Dampeners, KeyCode.Z);
		InputMappings.keyMappings.Add(InputBinding.BuildMode, KeyCode.B);
		InputMappings.keyMappings.Add(InputBinding.Overlay, KeyCode.Tab);
	}

	private class Mapping : Network.ISerialized
	{
		public InputBinding binding;
		public KeyCode key;

		public void FromMessage(Message message)
		{
			this.binding = (InputBinding)message.buffer.ReadInt();
			this.key = (KeyCode)message.buffer.ReadInt();
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteInt((int)this.binding);
			message.buffer.WriteInt((int)this.key);
		}
	}
}