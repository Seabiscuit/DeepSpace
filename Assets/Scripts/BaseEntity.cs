﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class BaseEntity : BaseNetworkable
{
	public static Dictionary<uint, MethodInfo> rpcTargets = new Dictionary<uint, MethodInfo>();
	public static HashSet<BaseEntity> saveList = new HashSet<BaseEntity>();
	public bool enableSaving = true;
	public Vector3 linearVelocity;
	[NonSerialized]
	public List<BaseEntity> children;
	private Bounds bounds;
	public BaseEntity.Flags flags;
	private Message message;
	private Quaternion targetRotation;
	private Vector3 targetPosition;
	private float timeSinceUpdate;

	protected override void OnNetworkInit()
	{
		base.OnNetworkInit();
	}

	protected override void OnLocalInit()
	{
		base.OnLocalInit();
		this.bounds = this.GetComponent<Collider>().bounds;
		this.children = new List<BaseEntity>(16);
	}

	public Bounds GetAxisAlignedBounds()
	{
		return this.bounds.Transform(this.GetCenter());
	}

	public Vector3 GetPivot()
	{
		return this.transform.position;
	}

	public Vector3 GetCenter()
	{
		return this.GetPivot() + this.transform.rotation * this.bounds.center;
	}

	public Vector3 GetClosestPoint(Vector3 position)
	{
		return this.GetAxisAlignedBounds().GetOBBClosestPoint(this.transform.rotation, position);
	}

	public float GetClosestDistance(Vector3 position)
	{
		return Vector3.Distance(position, this.GetClosestPoint(position));
	}

	public float GetClosestSqrDistance(Vector3 position)
	{
		return (position - this.GetClosestPoint(position)).sqrMagnitude;
	}

	public float GetClosestDistance(BaseEntity entity)
	{
		return this.GetClosestDistance(entity.GetClosestPoint(this.GetCenter()));
	}

	public float GetClosestSqrDistance(BaseEntity entity)
	{
		return this.GetClosestSqrDistance(entity.GetClosestPoint(this.GetCenter()));
	}

	public virtual ItemInventory GetInventory(uint inventoryId)
	{
		return null;
	}

	public virtual BaseItem GetItem(uint itemId)
	{
		return null;
	}

	public BaseEntity GetParent()
	{
		return this.transform.parent.GetComponent<BaseEntity>();
	}

	public bool HasParent()
	{
		return this.GetParent() != null;
	}

	public void SetParent(BaseEntity entity, bool worldPositionStays = false, bool globalUpdate = true)
	{
		if (entity != null && (entity == this || this.HasChild(entity)))
			return;
		BaseEntity parent = this.GetParent();
		if (parent != null)
			parent.children.Remove(this);
		if (entity == null)
		{
			this.transform.SetParent(null, worldPositionStays);
		}
		else
		{
			entity.children.Add(this);
			this.transform.SetParent(entity.transform, worldPositionStays);
		}
	}

	public bool HasChild(BaseEntity entity)
	{
		if (entity == this)
			return true;
		BaseEntity parent = entity.GetParent();
		if (parent != null)
			return this.HasChild(parent);
		return false;
	}

	public void OnTargetTransformUpdate(Vector3 position, Quaternion rotation)
	{
		this.targetRotation = rotation;
		this.targetPosition = position;
		this.timeSinceUpdate = 0.0f;
	}

	private void Update()
	{
		this.targetPosition = this.targetPosition + this.linearVelocity * Time.deltaTime;
		this.SnapTransformToEnd();
	}

	private void SnapTransformToEnd()
	{
		this.transform.position = this.targetPosition;
		this.transform.rotation = this.targetRotation;
	}

	public void SendRpcMessage<T1, T2, T3, T4>(uint rpcId, T1 arg1, T2 arg2, T3 arg3, T4 arg4, bool isReliable)
	{
		this.StartRpcMessage();
		this.message.buffer.WriteUint(rpcId);
		this.WriteRpcObject<T1>(arg1);
		this.WriteRpcObject<T2>(arg2);
		this.WriteRpcObject<T3>(arg3);
		this.WriteRpcObject<T4>(arg4);
		this.TransmitRpcMessage(isReliable);
	}

	public void SendRpcMessage<T1, T2, T3>(uint rpcId, T1 arg1, T2 arg2, T3 arg3, bool isReliable)
	{
		this.StartRpcMessage();
		this.message.buffer.WriteUint(rpcId);
		this.WriteRpcObject<T1>(arg1);
		this.WriteRpcObject<T2>(arg2);
		this.WriteRpcObject<T3>(arg3);
		this.TransmitRpcMessage(isReliable);
	}

	public void SendRpcMessage<T1, T2>(uint rpcId, T1 arg1, T2 arg2, bool isReliable)
	{
		this.StartRpcMessage();
		this.message.buffer.WriteUint(rpcId);
		this.WriteRpcObject<T1>(arg1);
		this.WriteRpcObject<T2>(arg2);
		this.TransmitRpcMessage(isReliable);
	}

	public void SendRpcMessage<T1>(uint rpcId, T1 arg1, bool isReliable)
	{
		this.StartRpcMessage();
		this.message.buffer.WriteUint(rpcId);
		this.WriteRpcObject<T1>(arg1);
		this.TransmitRpcMessage(isReliable);
	}

	public void SendRpcMessage(uint rpcId, bool isReliable)
	{
		this.StartRpcMessage();
		this.message.buffer.WriteUint(rpcId);
		this.TransmitRpcMessage(isReliable);
	}

	private void WriteRpcObject<T>(T arg)
	{
		this.message.buffer.WriteObject<T>(arg);
	}

	private void StartRpcMessage()
	{
		if (this.message != null || (this.peer.IsClient() && !BasePlayer.localPlayer.connection.IsReady))
			return;
		this.message = new Message();
		this.message.buffer.WriteType(Message.Type.Rpc);
		this.message.buffer.WriteUint(this.networkableId);
		this.message.buffer.message = this.message;
	}

	private void TransmitRpcMessage(bool reliable)
	{
		this.SendMessageToSubscribers(this.message, reliable);
		this.message.buffer.message = null;
		this.message = null;
	}

	public void EchoRpcMessage(Message message)
	{
		this.message = message;
		this.message.buffer.position = this.message.length;
		this.TransmitRpcMessage(true);
	}

	public override void Save(ref BaseNetworkable.SaveState state)
	{
		base.Save(ref state);
		state.networkable.entity = new Network.BaseEntity();
		state.networkable.entity.pos = this.transform.position;
		state.networkable.entity.rot = this.GetNetworkRotation().eulerAngles;
	}

	public override void Load(ref BaseNetworkable.SaveState state)
	{
		base.Load(ref state);
		if (state.networkable.entity != null)
		{
			Network.BaseEntity entity = state.networkable.entity;
			if (entity.pos.IsNaNOrInfinity())
			{
				Debug.LogWarning("Loading an entity with invalid position");
				entity.pos = Vector3.zero;
			}
			this.transform.localPosition = this.targetPosition = entity.pos;
			this.transform.localRotation = this.targetRotation = Quaternion.Euler(entity.rot);
		}
	}

	[Flags]
	public enum Flags
	{
	}

	public static void RegisterRpcTargets()
	{
		foreach (Type type in GameInstance.gameAssembly.GetTypes())
		{
			if (typeof(BaseEntity).IsAssignableFrom(type))
			{
				MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
				foreach (MethodInfo method in methods)
				{
					object attribute = method.GetCustomAttributes(typeof(BaseEntity.RpcTarget), false).FirstOrDefault();
					if (attribute == null)
						continue;
					BaseEntity.RpcTarget descriptor = attribute as BaseEntity.RpcTarget;
					uint rpcId = descriptor.rpcId;
					BaseEntity.rpcTargets.Add(rpcId, method);
				}
			}
		}
	}

	public class RpcTarget : Attribute
	{
		public readonly uint rpcId;

		public RpcTarget(uint rpcId)
		{
			this.rpcId = rpcId;
		}
	}
}