﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public readonly float unhoveredTransparency = 0.15f;
	public readonly float hoveredTransparency = 0.60f;
	public BuildingIcon.Flags flag;
	public string debugName;
	[NonSerialized]
	public BuildableDefinition definition;
	[NonSerialized]
	public Image background;
	public string prefabName;

	private void OnEnable()
	{
		this.background = this.GetComponent<Image>();
		if (this.prefabName == null || this.prefabName == string.Empty)
			return;
		this.definition = InspectorDefinition.singleton.GetDefinition<BuildableDefinition>(this.prefabName);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this.background.SetOpacity(this.hoveredTransparency);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		this.background.SetOpacity(this.unhoveredTransparency);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		PointerEventData.InputButton button = eventData.button;
		if (this.definition == null && button == PointerEventData.InputButton.Left)
			return;
		BasePlayer.localPlayer.planner.ChangeSelected(this.definition);
		if (button != PointerEventData.InputButton.Right)
			return;
		GeometricEditor.singleton.Focus(this);
	}

	public enum Flags
	{
	}
}