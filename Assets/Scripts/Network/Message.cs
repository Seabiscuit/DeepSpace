﻿using System;
using System.Collections;
using UnityEngine;

public class Message
{
	public Message.Type type;
	public Serializer buffer;
	public int length;

	public Message()
	{
		this.buffer = Serializer.GetPreallocated();
	}

	public class Handler : System.Attribute
	{
		public Message.Type type;

		public Handler(Message.Type type)
		{
			this.type = type;
		}
	}

	public enum Type : byte
	{
		Discovery = 1,
		Auth = 2,
		Ready = 3,
		Create = 4,
		Destroy = 5,
		Transform = 6,
		Update = 7,
		Rpc = 8,
		Close = 9
	}
}