﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Templates contain information about a 3d geometric shape.
// Attributes define properties assigned to templates.
// Effectors define how these properties are used to influence geometric shapes.
// Example: A cuboid geometric has a scale property that can be controlled by the user. It has a scale effector that uses information in the scale property that are used to influence the scale of the geometric.  

public class GeometricEditor : MonoBehaviour
{
	public static GeometricEditor singleton;
	private GeometricTarget targetBehavior;
	private MeshFilter targetFilter;
	private Camera viewCamera;
	private BuildableDefinition definition;

	private void Awake()
	{
		this.viewCamera = this.transform.GetComponentInChildren<Camera>();
		Debug.Assert(this.viewCamera != null, "No geometric view camera");
		this.targetBehavior = this.GetComponentInChildren<GeometricTarget>();
		GeometricEditor.singleton = this;
	}

	public void Focus(BuildingIcon icon)
	{
		this.Unfocus();
		if (this.viewCamera == null || icon == null)
			return;
		this.targetFilter.mesh = icon.definition.GetComponent<MeshFilter>().mesh;
		this.viewCamera.enabled = true;
		this.definition = icon.definition;
	}

	public void Unfocus()
	{
		this.viewCamera.enabled = false;
	}

	public abstract class BaseEffector
	{

	}

	public abstract class BaseTemplate
	{
		protected List<GeometricEditor.BaseAttribute> attributes = new List<GeometricEditor.BaseAttribute>(8);

		public abstract void InitAttributes();

		public GeometricEditor.BaseAttribute[] GetAttributes()
		{
			return this.attributes.ToArray();
		}
	}

	public abstract class BaseAttribute
	{
		public virtual float GetMinimum()
		{
			return 0.0f;
		}

		public virtual float GetMaximum()
		{
			return 0.0f;
		}

		public virtual GeometricEditor.BaseEffector GetEffector()
		{
			return null;
		}

		public float GetRange()
		{
			return this.GetMaximum() - this.GetMinimum();
		}
	}
}