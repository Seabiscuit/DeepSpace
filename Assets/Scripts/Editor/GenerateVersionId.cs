﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class GenerateVersionId : IPostprocessBuildWithReport
{
	public int callbackOrder
	{
		get
		{
			return 0;
		}
	}

	public void OnPostprocessBuild(BuildReport report)
	{
		using (MemoryStream stream = new MemoryStream(50))
		{
			string id = report.summary.buildEndedAt.ToString("yyMMddHHmmss");
			byte[] data = Encoding.ASCII.GetBytes(id);
			stream.Write(data, 0, data.Length);
			File.WriteAllBytes("Builds/.version", stream.ToArray());
			Debug.Log("Generated DeepSpace build id - " + id);
		}
	}
}