﻿using UnityEngine;

[ExecuteInEditMode]
public class PlanetShader : MonoBehaviour
{
	private float hdrExposure = 1.0f;
	private Color atmoColor = new Color(0, 0, 0);
	private float atmoStrength = 10.0f;

	private float kr = 0.0025f;
	private float km = 0.0010f;
	private float outerScaleFactor = 1.015f;
	private float innerRadius;
	private float outerRadius;
	private float scaleDepth = 0.25f;
	private float scale;
	private float gamma = 1.0f;

	private Material material;
	private Transform[] shadow = new Transform[10];
	private int shadowNumber = 0;
	// only 10 planet shadow allowed (hard coded in shader) but you can state less for better performance

	private void Awake()
	{
		this.material = this.GetComponent<MeshRenderer>().sharedMaterials[0];
		if (QualitySettings.activeColorSpace == ColorSpace.Gamma)
			this.gamma = 2.2f;
		this.innerRadius = transform.localScale.x;
		this.outerRadius = outerScaleFactor * transform.localScale.x;
		this.scale = 1.0f / (outerRadius - innerRadius);
		this.SetMaterial();
	}

	public void SetShadowNumber(int value)
	{
		value = (int)Mathf.Clamp(value, 0, 10);
		this.shadowNumber = value;
	}

	public void SetShadow(Transform parent, int value)
	{
		if (value < 10 && value >= 0)
		{
			shadow[value] = parent;
			this.SetMaterial();
		}
	}

	private void SetMaterial()
	{
		if (material == null)
			return;
		this.InitMaterial(material);
	}

	private void LateUpdate()
	{
		this.SetMaterial();
	}

	private void InitMaterial(Material material)
	{
		string[] keyword = new string[2];
		keyword[0] = "MOBILE_OFF";
		keyword[1] = "ATMO_OFF";
		material.shaderKeywords = keyword;
		Vector3 invWL4 = new Vector3(1 - atmoColor.linear.r, 1 - atmoColor.linear.g, 1 - atmoColor.linear.b);
		invWL4 = new Vector3(1.0f / Mathf.Pow(invWL4.x, 4),
							 1.0f / Mathf.Pow(invWL4.y, 4),
							 1.0f / Mathf.Pow(invWL4.z, 4));
		material.SetFloat("_Gamma", gamma);
		material.SetVector("v3InvWavelength", invWL4);
		material.SetFloat("fOuterRadius", outerRadius);
		material.SetFloat("fInnerRadius", innerRadius);
		material.SetFloat("fKrESun", kr * atmoStrength);
		material.SetFloat("fKmESun", km * atmoStrength);
		material.SetFloat("fKr4PI", kr * 4.0f * Mathf.PI);
		material.SetFloat("fKm4PI", km * 4.0f * Mathf.PI);
		material.SetFloat("fScale", scale);
		material.SetFloat("fScaleDepth", scaleDepth);
		material.SetFloat("fScaleOverScaleDepth", scale / scaleDepth);
		material.SetFloat("fHdrExposure", hdrExposure);
		material.SetVector("v3Translate", transform.position);
		material.SetFloat("shadowNumber", shadowNumber);
		int i = -1;
		while (++i < this.shadowNumber && this.shadow[i])
		{
			material.SetVector("planetShadowPos" + i, shadow[i].position);
			material.SetFloat("planetShadowSca" + i, shadow[i].localScale.x);
		}
	}
}