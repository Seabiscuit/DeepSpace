﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager
{
	public static readonly AssetManager singleton = new AssetManager();
	private string bundlePath = "bundles/export";
	public Transform prefabs;
	private AssetBundleManifest assetManifest;
	private AssetBundle assetBundle;
	public bool cacheEnabled = true;

	private static class AssetCache<TAsset> where TAsset : UnityEngine.Object
	{
		public static Dictionary<string, TAsset> assetsByFilePath = new Dictionary<string, TAsset>();
	}

	public void LoadAssetBundle(string filePath)
	{
		this.assetBundle = AssetBundle.LoadFromFile(filePath);
		if (this.assetBundle != null)
			this.assetManifest = this.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
		else
			UnityEngine.Debug.LogError("Failed to load exported assets");
	}

	public bool TryGetAssetWithoutLoad<TAsset>(string filePath, out TAsset asset) where TAsset : UnityEngine.Object
	{
		asset = null;
		if (!this.IsLoaded<TAsset>(filePath))
			return false;
		asset = this.LoadAsset<TAsset>(filePath);
		return true;
	}

	public TAsset GetAsset<TAsset>(string filePath) where TAsset : UnityEngine.Object
	{
		if (this.IsLoaded<TAsset>(filePath))
			return AssetCache<TAsset>.assetsByFilePath[filePath];
		return this.LoadAsset<TAsset>(filePath);
	}

	public bool IsLoaded<TAsset>(string filePath) where TAsset : UnityEngine.Object
	{
		return AssetCache<TAsset>.assetsByFilePath.ContainsKey(filePath);
	}

	private TAsset LoadAsset<TAsset>(string filePath) where TAsset : UnityEngine.Object
	{
		if (this.assetBundle == null)
			return null;
		TAsset asset = this.assetBundle.LoadAsset<TAsset>(filePath);
		if (asset != null && this.cacheEnabled && !this.IsLoaded<TAsset>(filePath))
			AssetCache<TAsset>.assetsByFilePath.Add(filePath, asset);
		return asset;
	}

	public GameObject[] LoadAllPrefabs()
	{
		if (this.assetBundle == null)
			return null;
		string[] filePaths = this.assetBundle.GetAllAssetNames();
		List<GameObject> prefabs = new List<GameObject>();
		foreach (string filePath in filePaths)
		{
			if (filePath.StartsWith("assets/prefabs/"))
				prefabs.Add(this.LoadPrefab(filePath));
		}
		return prefabs.ToArray();
	}

	public GameObject LoadPrefab(string filePath)
	{
		if (string.IsNullOrEmpty(filePath))
			return null;
		GameObject prefab = this.LoadAsset<GameObject>(filePath);
		MeshFilter filter = prefab.GetComponent<MeshFilter>();
		if (filter != null && filter.sharedMesh != null)
			filter.sharedMesh.RecalculateBounds();
		prefab.SetActive(false);
		InspectorDefinition[] definitions = prefab.GetComponents<InspectorDefinition>();
		if (definitions != null && definitions.Length > 0)
		{
			GameObject copy = MonoBehaviour.Instantiate<GameObject>(prefab);
			foreach (InspectorDefinition definition in copy.GetComponents<InspectorDefinition>())
				definition.Load(filePath);
			foreach (Component component in copy.GetComponents<Component>())
			{
				Type type = component.GetType();
				if (typeof(InspectorDefinition).IsAssignableFrom(type) || typeof(Transform).IsAssignableFrom(type) || (type == typeof(MeshFilter)))
					continue;
				MonoBehaviour.Destroy(component);
			}
			copy.transform.SetParent(this.prefabs);
		}
		return prefab;
	}
}