﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	private static Dictionary<BaseItem, ItemSlot> itemsToSlots = new Dictionary<BaseItem, ItemSlot>();
	public readonly Vector3 unhoveredScale = new Vector3(1.00f, 1.00f, 1.00f);
	public readonly Vector3 hoveredScale = new Vector3(0.95f, 0.95f, 0.95f);
	public string debugName;
	public ItemSlot.Flags flag;
	public ItemInventory inventory;
	public BaseItem item;
	[NonSerialized]
	public OverlayController overlay;
	private float unselectedOpacity = 0.078f;
	private float selectedOpacity = 0.274f;
	private bool selected;
	[NonSerialized]
	public Image image;
	[NonSerialized]
	public Image icon;
	[NonSerialized]
	public Text label;
	public int index;

	private void OnEnable()
	{
		this.overlay = MonoBehaviour.FindObjectOfType<OverlayController>();
		this.label = this.GetComponentInChildren<Text>();
		Debug.Assert(this.label != null, "ItemSlot has no text component");
		this.image = this.GetComponent<Image>();
		GameObject obj = new GameObject("Icon");
		obj.transform.SetParent(this.transform, false);
		this.icon = obj.AddComponent<Image>();
		this.icon.sprite = this.GetSprite();
	}

	public void UpdateItemSlot(BaseItem item)
	{
		this.OnItemChanged(item);
		this.OnItemQuantityChanged();
	}

	private void OnItemChanged(BaseItem item)
	{
		if (this.item == item && this.item != null)
			return;
		BaseItem oldItem = this.item;
		if (item == null && ItemSlot.itemsToSlots.ContainsValue(this))
			ItemSlot.itemsToSlots.Remove(oldItem);
		else if (item != null)
			ItemSlot.itemsToSlots[item] = this;
		this.item = item;
		this.icon.sprite = this.GetSprite();
	}

	private Sprite GetSprite()
	{
		Texture2D texture = new Texture2D(1, 1);
		texture.wrapMode = TextureWrapMode.Repeat;
		if (this.item != null)
			texture.SetPixel(0, 0, new Color(1.0f, 1.0f, 1.0f, 0.65f));
		else
			texture.SetPixel(0, 0, Color.clear);
		texture.Apply();
		return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
	}

	private void OnItemQuantityChanged()
	{
		string text;
		if (this.item == null || (this.item != null && this.item.quantity == 1))
			text = string.Empty;
		else
			text = this.item.quantity.ToString();
		this.label.text = text;
	}

	public void ChangeSelected(bool select)
	{
		if (this.selected == select)
			return;
		this.selected = select;
		if (this.selected)
			this.image.SetOpacity(this.selectedOpacity);
		else
			this.image.SetOpacity(this.unselectedOpacity);
	}

	private bool LocalOverlayVisible()
	{
		return this.overlay.tab == OverlayController.Tab.Inventory && this.overlay.isDrawn;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.overlay.IsUsingPointer() && this.LocalOverlayVisible())
			this.transform.localScale = this.hoveredScale;
		else
			this.OnPointerExit(eventData);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		this.transform.localScale = this.unhoveredScale;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
	}

	public void OnDrag(PointerEventData eventData)
	{
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		PointerEventData.InputButton button = eventData.button;
		if (this.item == null || !this.overlay.IsUsingPointer() || !this.LocalOverlayVisible() || !eventData.dragging || !eventData.pointerCurrentRaycast.isValid)
			return;
		GameObject target = eventData.pointerCurrentRaycast.gameObject;
		ItemSlot slot = target.GetComponentInParent<ItemSlot>();
		if (slot == null)
			return;
		int quantity = this.item.quantity;
		if (button == PointerEventData.InputButton.Left)
			this.MoveItems(quantity, slot.inventory, slot.index);
		else
			this.MoveItems(quantity / 2, slot.inventory, slot.index);
	}

	public void MoveItems(int quantity, ItemInventory targetInventory, int targetIndex)
	{
		if (this.item == null || this.item.inventory == null)
			return;
		ItemInventory inventory = this.item.inventory;
		if (BasePlayer.localPlayer == null)
			return;
		BasePlayer.localPlayer.AskForMoveItem(this.item.itemId, quantity, targetInventory.inventoryId, targetIndex);
	}

	[Flags]
	public enum Flags
	{
		None = 0,
		Hotbar = 1,
		Unassigned3 = 2,
		Unassigned2 = 4,
		Unassigned1 = 8
	}
}