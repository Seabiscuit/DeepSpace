﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
	public class BaseItem : ISerialized
	{
		public uint itemId;
		public int stackable;
		public int quantity;

		public void FromMessage(Message message)
		{
			this.itemId = message.buffer.ReadUint();
			this.stackable = message.buffer.ReadInt();
			this.quantity = message.buffer.ReadInt();
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteUint(this.itemId);
			message.buffer.WriteInt(this.stackable);
			message.buffer.WriteInt(this.quantity);
		}
	}
}