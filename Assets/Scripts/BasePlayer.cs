﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePlayer : BaseEntity
{
	public static BasePlayer localPlayer;
	public string displayName;
	public ulong playerID;
	private float lastClientUpdateTime = float.NegativeInfinity;
	private float clientUpdateRate = 60.0f;
	public Connection connection;
	[NonSerialized]
	public PlayerEyes.ViewMode viewMode;
	private Dictionary<string, GameObject> overlayElements = new Dictionary<string, GameObject>();
	private OverlayController overlay;
	private Quaternion rotateDelta;
	private Vector3 moveDelta;
	private float dampenersThreshold;
	private float thresholdTime;
	private bool motionDampeners;
	private BaseEntity controlled;
	[NonSerialized]
	public BuildingPlanner planner;
	[NonSerialized]
	public PlayerHotbar hotbar;
	[NonSerialized]
	public ItemInventory body;
	[NonSerialized]
	public PlayerEyes eyes;
	private InputSnapshot currentSnapshot;
	private InputSnapshot lastSnapshot;
	private bool isSleeping;

	public void InitLocalPlayer()
	{
		BasePlayer.localPlayer = this;
		this.gameObject.name = "LocalPlayer";
		this.overlay = OverlayController.singleton;
		this.InitPlayerOverlay();
		this.InitPlayerEyes();
	}

	private void InitPlayerInventory()
	{
		this.hotbar.inventory.Init(8);
		this.hotbar.inventory.owner = this;
		this.body.Init(32);
		this.body.owner = this;
		BaseItem item = new BaseItem();
		item.itemId = GameInstance.singleton.AllocateId();
		item.quantity = 1;
		this.hotbar.inventory.Add(item);
	}

	private void InitPlayerEyes()
	{
		this.viewMode = PlayerEyes.ViewMode.FirstPerson;
		this.eyes = this.gameObject.AddComponent<PlayerEyes>();
		this.eyes.InitPlayerEyes(this);
		this.eyes.body = this.transform;
		this.eyes.SetViewModeFirstPerson();
	}

	private void InitPlayerOverlay()
	{
		GameObject hotbar = GameInstance.singleton.SpawnPrefab("assets/prefabs/player/hotbar.prefab", this.overlay.transform);
		InventoryGrid hotbarGrid = hotbar.GetComponent<InventoryGrid>();
		hotbarGrid.Init(this.hotbar.inventory);
		this.overlay.localPlayer = this;
		this.overlayElements.Add(hotbarGrid.debugName, hotbar);
	}

	protected override void OnNetworkInit()
	{
		base.OnNetworkInit();
		this.InitPlayerInventory();
		this.InitPlayerEyes();
	}

	protected override void OnLocalInit()
	{
		base.OnLocalInit();
		this.planner = new BuildingPlanner(this);
		this.hotbar = new PlayerHotbar(this);
		this.body = new ItemInventory();
		this.motionDampeners = true;
		this.dampenersThreshold = 0.0f;
		this.controlled = this;
	}

	public override ItemInventory GetInventory(uint inventoryId)
	{
		if (this.hotbar != null && this.hotbar.inventory.inventoryId == inventoryId)
			return this.hotbar.inventory;
		else if (this.body != null && this.body.inventoryId == inventoryId)
			return this.body;
		return null;
	}

	public override BaseItem GetItem(uint itemId)
	{
		return this.GetPlayerItem(itemId);
	}

	public BaseItem GetPlayerItem(uint itemId)
	{
		if (this.hotbar != null)
		{
			BaseItem item = this.hotbar.inventory.GetItemById(itemId);
			if (item != null)
				return item;
		}
		if (this.body != null)
		{
			BaseItem item = this.body.GetItemById(itemId);
			if (item != null)
				return item;
		}
		return null;
	}

	public void AskForMoveItem(uint itemId, int quantity, uint targetInventoryId, int targetIndex)
	{
		if (!this.connection.IsReady)
			return;
		this.SendRpcMessage<uint, int, uint, short>(681695853U, itemId, quantity, targetInventoryId, (short)targetIndex, true);
	}

	public void UpdateLocalPlayer()
	{
		this.lastSnapshot = this.currentSnapshot;
		this.currentSnapshot = new InputSnapshot();
		this.currentSnapshot.Capture(this.lastSnapshot);
		this.ClientInput(this.currentSnapshot);
		if (UnityEngine.Time.realtimeSinceStartup - this.lastClientUpdateTime > (1.0f / this.clientUpdateRate))
			this.SendClientUpdate();
		this.OnTargetTransformUpdate(this.transform.position, this.transform.rotation);
	}

	private void SendClientUpdate()
	{
		if (this.connection == null)
			return;
		this.lastClientUpdateTime = UnityEngine.Time.realtimeSinceStartup;
		Network.PlayerUpdate update = new Network.PlayerUpdate();
		update.bodyPosition = this.transform.position;
		update.bodyRotation = this.transform.rotation;
		Message message = new Message();
		message.buffer.WriteType(Message.Type.Update);
		update.ToMessage(message);
		this.peer.SendMessage(message, this.connection, true);
	}

	public ulong GetClientId()
	{
		if (this.connection == null)
			return 0UL;
		return this.connection.networkId;
	}

	public override bool IsPlayer(ulong clientId)
	{
		if (this.connection == null)
			return false;
		return this.connection.networkId == clientId;
	}

	public bool WasJustPressed(InputBinding binding)
	{
		if (this.IsDown(binding))
			return !this.WasDown(binding);
		return false;
	}

	public bool WasJustReleased(InputBinding binding)
	{
		if (!this.IsDown(binding))
			return this.WasDown(binding);
		return false;
	}

	public bool IsDown(InputBinding binding)
	{
		return this.currentSnapshot.IsDown(binding);
	}

	public bool WasDown(InputBinding binding)
	{
		return this.currentSnapshot.WasDown(binding);
	}

	public void ClientInput(InputSnapshot snapshot)
	{
		this.OverlayInput(snapshot);
		this.hotbar.ClientInput(snapshot);
		this.planner.ClientInput(snapshot);
		if (this.CanRotate())
			this.OrientationInput(snapshot);
		this.MovementInput(snapshot);
	}

	private void OverlayInput(InputSnapshot snapshot)
	{
		if (this.overlay.isOpen)
			this.overlay.ClientInput();
		if (!snapshot.WasJustPressed(InputBinding.Overlay))
			return;
		this.overlay.Toggle();
	}

	public bool CanInteract()
	{
		return this.CanRotate();
	}

	public bool CanRotate()
	{
		return this.isSleeping == false && this.overlay.IsUsingPointer() == false;
	}

	private void OrientationInput(InputSnapshot snapshot)
	{
		this.rotateDelta = Quaternion.identity;
		float pitch = -snapshot.GetMouseDelta().y;
		float yaw = snapshot.GetMouseDelta().x;
		float roll = snapshot.IsDown(InputBinding.RollLeft) ? 1.0f : (snapshot.IsDown(InputBinding.RollRight) ? -1.0f : 0.0f);
		if (Math.Abs(pitch) > float.Epsilon)
			this.rotateDelta = this.rotateDelta * Quaternion.AngleAxis(pitch * 3.0f, Vector3.right);
		if (Math.Abs(yaw) > float.Epsilon)
			this.rotateDelta = this.rotateDelta * Quaternion.AngleAxis(yaw * 3.0f, Vector3.up);
		if (Math.Abs(roll) > float.Epsilon)
			this.rotateDelta = this.rotateDelta * Quaternion.AngleAxis(roll * 1.2f, Vector3.forward);
		if (this.rotateDelta == Quaternion.identity)
			return;
		this.transform.rotation = this.transform.rotation * this.rotateDelta.normalized;
	}

	private void MovementInput(InputSnapshot snapshot)
	{
		this.moveDelta = Vector3.zero;
		if (this.controlled == null)
			return;
		float acceleration = 8.0f;
		if (snapshot.WasJustPressed(InputBinding.Dampeners))
			this.motionDampeners = this.motionDampeners.Invert();
		float Z = snapshot.IsDown(InputBinding.Forward) ? 1.0f : (snapshot.IsDown(InputBinding.Backward) ? -1.0f : 0.0f);
		float X = snapshot.IsDown(InputBinding.StrafeRight) ? 1.0f : (snapshot.IsDown(InputBinding.StrafeLeft) ? -1.0f : 0.0f);
		float Y = snapshot.IsDown(InputBinding.Up) ? 1.0f : (snapshot.IsDown(InputBinding.Down) ? -1.0f : 0.0f);
		if (this.motionDampeners && Math.Abs(Z) < float.Epsilon && Math.Abs(X) < float.Epsilon && Math.Abs(Y) < float.Epsilon)
			this.moveDelta = -this.linearVelocity;
		this.moveDelta = this.moveDelta + this.transform.forward * Z;
		this.moveDelta = this.moveDelta + this.transform.right * X;
		this.moveDelta = this.moveDelta + this.transform.up * Y;
		if (this.moveDelta == Vector3.zero)
			return;
		this.linearVelocity += this.moveDelta.normalized * acceleration * Time.deltaTime;
		this.thresholdTime = this.thresholdTime + Time.deltaTime;
		if (this.linearVelocity.sqrMagnitude >= this.dampenersThreshold && this.thresholdTime > 0.35f)
			return;
		this.linearVelocity = Vector3.zero;
		this.thresholdTime = 0.0f;
	}

	[BaseEntity.RpcTarget(681695853U)]
	private void DoMoveItem(BasePlayer player, Message message)
	{
		uint itemId = message.buffer.ReadUint();
		int quantity = message.buffer.ReadInt();
		uint targetInventoryId = message.buffer.ReadUint();
		int targetIndex = message.buffer.ReadShort();
		BaseItem item = this.GetPlayerItem(itemId);
		if (item == null)
			return;
		ItemInventory target = this.GetInventory(targetInventoryId);
		if (target == null)
		{
			Debug.LogWarning("Player is requesting item move to invalid inventory");
			return;
		}
		if (target.TryMerge(item, quantity, targetIndex) != quantity)
			return;
		if (this.peer.IsClient())
			return;
		this.EchoRpcMessage(message);
	}

	[BaseEntity.RpcTarget(463457298U)]
	private void DoBuild(BasePlayer player, Message message)
	{
		if (this.peer == null || this.peer.IsClient())
			return;
		Network.BuildingInfo info = new Network.BuildingInfo();
		info.FromMessage(message);
		if (info.prefabName == string.Empty)
			return;
		if (this.planner == null)
		{
			Debug.LogWarning("Planner was null");
			return;
		}
		this.planner.DoBuild(info);
	}

	public override void Save(ref BaseNetworkable.SaveState state)
	{
		base.Save(ref state);
		state.networkable.player = new Network.BasePlayer();
		state.networkable.player.clientId = this.GetClientId();
		state.networkable.player.name = this.displayName;
		state.networkable.player.hotbar = this.hotbar.inventory.Save();
		state.networkable.player.body = this.body.Save();
	}

	public override void Load(ref BaseNetworkable.SaveState state)
	{
		base.Load(ref state);
		if (state.networkable.player != null)
		{
			Network.BasePlayer player = state.networkable.player;
			this.playerID = player.clientId;
			this.displayName = player.name;
			if (player.hotbar != null)
				this.hotbar.inventory.Load(player.hotbar);
			if (player.body != null)
				this.body.Load(player.body);
		}
	}
}