﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ImageEx
{
	public static void SetOpacity(this Image image, float opacity)
	{
		Debug.Assert(opacity < 1.0001f && opacity >= 0.0f, "Opacity out of range");
		Color color = image.color;
		color.a = opacity;
		image.color = color;
	}
}