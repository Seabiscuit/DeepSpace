﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Network
{
	public class PlayerUpdate : ISerialized
	{
		public Vector3 bodyPosition;
		public Quaternion bodyRotation;

		public void FromMessage(Message message)
		{
			this.bodyPosition = message.buffer.ReadVector3();
			this.bodyRotation = message.buffer.ReadVector3().ToQuaternion();
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteVector3(this.bodyPosition);
			message.buffer.WriteVector3(this.bodyRotation.eulerAngles);
		}
	}
}