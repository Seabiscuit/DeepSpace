﻿using LiteNetLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Connection
{
	public ulong networkId;
	public string displayName;
	public string ipAddress;
	public NetPeer peer;
	public bool isAuthenicated;
	public BasePlayer player;

	public bool IsReady { get; set; }

	public bool HasPlayer
	{
		get
		{
			return this.player != null;
		}
	}

	public Connection(NetPeer peer)
	{
		this.peer = peer;
		this.ipAddress = peer.EndPoint.Address.ToString();
		this.isAuthenicated = false;
	}

	public bool HasDisplayName()
	{
		return this.displayName != null && this.displayName != string.Empty;
	}

	internal void Close()
	{
		this.peer.Disconnect();
	}
}