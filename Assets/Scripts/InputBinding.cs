﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Flags]
public enum InputBinding
{
	Forward = 1,
	Backward = 2,
	StrafeRight = 4,
	StrafeLeft = 8,
	Up = 16,
	Down = 32,
	RollRight = 64,
	RollLeft = 128,
	Dampeners = 256,
	BuildMode = 512,
	Overlay = 1024
}