﻿using LiteNetLib;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Server : Peer
{
	public IServerCallback callback;
	public string ipAddress;
	public int serverPort;
	public ushort serverId;
	public string serverName;
	private List<Connection> connectionsWithAuth;
	public int maxConnections;
	private Dictionary<ulong, Connection> connectionsById;
	public bool isActive;

	public bool Start(int maxConnections, ushort serverId)
	{
		Debug.Assert(!this.isActive, "Server is already started");
		if (this.isActive)
			return false;
		this.ipAddress = null;
		this.isServer = true;
		this.networkId = this.serverId = serverId;
		this.serverName = "Debug";
		this.serverPort = 7575;
		this.connectionsWithAuth = new List<Connection>();
		this.maxConnections = maxConnections;
		this.connectionsById = new Dictionary<ulong, Connection>();
		if (this.network == null || !this.network.Start(this.serverPort))
			return false;
		this.isActive = true;
		return true;
	}

	public Connection GetConnection(ulong clientId)
	{
		Connection connection;
		if (this.connectionsById.TryGetValue(clientId, out connection))
			return connection;
		return null;
	}

	public void Kick(ulong clientId, string reason)
	{
		if (!this.isActive)
			return;
		Connection connection = this.GetConnection(clientId);
		if (connection == null || connection.peer == null)
			return;
		Message message = new Message();
		message.buffer.WriteType(Message.Type.Close);
		message.buffer.WriteString(reason == null ? "" : reason);
		this.SendMessage(message, connection, true);
		connection.peer.Disconnect();
	}

	public override void OnMessageReceived(Message message, Connection sender)
	{
		if (this.callback != null)
			this.callback.OnMessageReceived(message, sender);
	}

	public override void OnPeerConnected(Connection connection)
	{
		Message message = new Message();
		message.buffer.WriteByte((byte)Message.Type.Discovery);
		message.buffer.WriteUshort(this.serverId);
		message.buffer.WriteString("Debug");
		this.SendMessage(message, connection, true);
	}

	public override void OnPeerDisconnect(Connection connection, string reason)
	{
		if (this.callback != null)
			this.callback.OnPeerDisconnected(connection, reason);
		this.OnPeerDeauth(connection);
	}

	public void OnPeerAuth(Connection connection, ulong networkId)
	{
		connection.networkId = networkId;
		connection.isAuthenicated = true;
		this.connectionsWithAuth.Add(connection);
		this.connectionsById.Add(networkId, connection);
		if (this.callback != null)
			this.callback.OnPeerAuth(connection);
	}

	public void OnPeerDeauth(Connection connection)
	{
		if (!connection.isAuthenicated)
			return;
		ulong oldValue = connection.networkId;
		connection.networkId = 0;
		connection.displayName = string.Empty;
		connection.isAuthenicated = false;
		this.connectionsWithAuth.Remove(connection);
		this.connectionsById.Remove(oldValue);
	}

	public bool Stop(bool sendMessages, string shutdownMessage)
	{
		Debug.Assert(this.isActive, "Server is stopped");
		if (this.network == null || !this.isActive)
			return false;
		this.connectionsWithAuth = null;
		this.connectionsById = null;
		this.network.Stop(sendMessages);
		this.isActive = false;
		this.network = null;
		return true;
	}
}

public interface IServerCallback
{
	void OnPeerDisconnected(Connection connection, string reason);
	void OnPeerAuth(Connection connection);

	void OnMessageReceived(Message message, Connection sender);
}