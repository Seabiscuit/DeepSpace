﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoundsEx
{
	public static Bounds Transform(this Bounds bounds, Vector3 center)
	{
		bounds.center = center;
		return bounds;
	}

	private static void GetAxisClosestPoint(Vector3 axis, ref Vector3 position, Vector3 diff, float extent)
	{
		float R = Vector3.Dot(diff, axis);
		if (R > extent)
			position = position + axis * extent;
		else if (R < -extent)
			position = position - axis * extent;
		else
			position = position + axis * R;
	}

	public static Vector3 _GetOBBClosestPoint(this Bounds bounds, Quaternion rotation, Vector3 position)
	{
		Vector3 closest = bounds.center;
		BoundsEx.GetAxisClosestPoint(rotation * Vector3.right, ref closest, position - closest, bounds.extents.x);
		BoundsEx.GetAxisClosestPoint(rotation * Vector3.up, ref closest, position - closest, bounds.extents.y);
		BoundsEx.GetAxisClosestPoint(rotation * Vector3.forward, ref closest, position - closest, bounds.extents.z);
		return position;
	}

	public static Vector3 GetOBBClosestPoint(this Bounds bounds, Quaternion rotation, Vector3 position)
	{
		bool flag1 = false;
		bool flag2 = false;
		bool flag3 = false;
		Vector3 right = rotation * Vector3.right;
		Vector3 up = rotation * Vector3.up;
		Vector3 forward = rotation * Vector3.forward;
		Vector3 extents = bounds.extents;
		Vector3 center = bounds.center;
		Vector3 diff = position - center;
		float num1 = Vector3.Dot(diff, right);
		Vector3 vector3_1;
		if ((double)num1 > (double)extents.x)
			vector3_1 = center + right * extents.x;
		else if ((double)num1 < -(double)extents.x)
		{
			vector3_1 = center - right * extents.x;
		}
		else
		{
			flag1 = true;
			vector3_1 = center + right * num1;
		}
		float num2 = Vector3.Dot(diff, rotation * Vector3.up);
		Vector3 vector3_2;
		if ((double)num2 > (double)extents.y)
			vector3_2 = vector3_1 + up * extents.y;
		else if ((double)num2 < -(double)extents.y)
		{
			vector3_2 = vector3_1 - up * extents.y;
		}
		else
		{
			flag2 = true;
			vector3_2 = vector3_1 + up * num2;
		}
		float num3 = Vector3.Dot(diff, rotation * Vector3.forward);
		Vector3 vector3_3;
		if ((double)num3 > (double)extents.z)
			vector3_3 = vector3_2 + forward * extents.z;
		else if ((double)num3 < -(double)extents.z)
		{
			vector3_3 = vector3_2 - forward * extents.z;
		}
		else
		{
			flag3 = true;
			vector3_3 = vector3_2 + forward * num3;
		}
		if (flag1 && flag2 && flag3)
			return position;
		return vector3_3;
	}
}