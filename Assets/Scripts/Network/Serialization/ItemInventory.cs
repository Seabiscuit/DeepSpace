﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
	public class ItemInventory : ISerialized
	{
		public BaseItem[] items;
		public uint inventoryId;
		public int capacity;
		public uint owner;

		public void FromMessage(Message message)
		{
			this.inventoryId = message.buffer.ReadUint();
			this.capacity = message.buffer.ReadInt();
			this.items = new Network.BaseItem[this.capacity];
			this.owner = message.buffer.ReadUint();
			for (int index = 0; index < this.capacity; index++)
			{
				Network.BaseItem item = new Network.BaseItem();
				if (message.buffer.ReadBool())
				{
					item.FromMessage(message);
					this.items[index] = item;
				}
			}
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteUint(this.inventoryId);
			message.buffer.WriteInt(this.capacity);
			message.buffer.WriteUint(this.owner);
			for (int index = 0; index < this.items.Length; index++)
			{
				Network.BaseItem item = this.items[index];
				message.buffer.WriteBool(item != null);
				if (item != null)
				{
					item.ToMessage(message);
				}
			}
		}
	}
}