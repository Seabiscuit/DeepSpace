﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBuildable : BaseEntity
{
	[NonSerialized]
	public BuildableDefinition definition;
	[NonSerialized]
	public ulong ownerId;
}