﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
	public class BaseEntity : ISerialized
	{
		public Vector3 pos;
		public Vector3 rot;

		public void FromMessage(Message message)
		{
			this.pos = message.buffer.ReadVector3();
			this.rot = message.buffer.ReadVector3();
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteVector3(this.pos);
			message.buffer.WriteVector3(this.rot);
		}
	}
}