﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Network
{
	public interface ISerialized
	{
		void FromMessage(Message message);
		void ToMessage(Message message);
	}

	public class Networkable
	{
		public Network.BaseEntity entity;
		public Network.BasePlayer player;
		public uint networkableId;
		public string prefabName;

		public void Serialize(Message message)
		{
			message.buffer.WriteUint(this.networkableId);
			message.buffer.WriteString(this.prefabName);
			message.buffer.WriteBool(this.entity != null);
			if (this.entity != null)
			{
				this.entity.ToMessage(message);
			}
			message.buffer.WriteBool(this.player != null);
			if (this.player != null)
			{
				this.player.ToMessage(message);
			}
		}

		public void Deserialize(Message message)
		{
			this.networkableId = message.buffer.ReadUint();
			this.prefabName = message.buffer.ReadString();
			if (message.buffer.ReadBool())
			{
				this.entity = new Network.BaseEntity();
				this.entity.FromMessage(message);
			}
			if (message.buffer.ReadBool())
			{
				this.player = new Network.BasePlayer();
				this.player.FromMessage(message);
			}
		}
	}
}