﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPlanner
{
	private readonly BasePlayer player;
	private BuildableDefinition selected;
	private BaseBuildable buildablePreview;
	private Material placementValid;
	private Material placementExpenses;
	private Material placementBlocked;
	private BuildingPlanner.PlacementInfo info;
	private float previewDistance;
	public bool inBuildMode;

	public BuildingPlanner(BasePlayer player)
	{
		this.player = player;
	}

	public void ClientInput(InputSnapshot snapshot)
	{
		if (snapshot.WasJustPressed(InputBinding.BuildMode))
			this.inBuildMode = !this.inBuildMode;
		if (UnityEngine.Input.GetMouseButtonDown(0))
			this.AskForBuild();
		this.UpdatePreview();
		this.info.prefabName = this.selected == null ? string.Empty : this.selected.prefabName;
		if (!this.inBuildMode || !this.HasPreview())
			return;
		this.previewDistance = Mathf.Clamp(this.previewDistance + snapshot.GetMouseDelta().z * 0.90f, 0.70f, this.selected.maxPlaceDistance);
	}

	public bool NeedsPreview()
	{
		return this.selected != null && this.inBuildMode;
	}

	public bool HasPreview()
	{
		return this.buildablePreview != null;
	}

	private void UpdatePreview()
	{
		if (!this.NeedsPreview())
		{
			this.DestroyPreview();
			return;
		}
		else if (!this.HasPreview())
		{
			this.buildablePreview = GameInstance.singleton.CreateEntity(this.selected.prefabName) as BaseBuildable;
			this.buildablePreview.definition = this.selected;
			MeshRenderer renderer = this.buildablePreview.GetComponent<MeshRenderer>();
			renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			renderer.receiveShadows = false;
			foreach (ConstructionPoint point in this.buildablePreview.GetComponentsInChildren<ConstructionPoint>())
				point.gameObject.layer = 0;
			this.buildablePreview.transform.SetParent(this.player.eyes.body);
			this.ResetPreview();
		}
		Ray mouse = this.player.eyes.view.ScreenPointToRay(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
		Ray ray = this.player.eyes.EyesRay();
		Vector3 position = ray.GetPoint(this.previewDistance);
		RaycastHit hit;
		if (Physics.Raycast(mouse.origin, mouse.direction, out hit, this.selected.maxPlaceDistance, 65536, QueryTriggerInteraction.UseGlobal))
			this.info.point = hit.collider.GetComponent<ConstructionPoint>();
		else
			this.info.point = null;
		if (this.info.point != null)
			position = this.info.point.GetPlacementPosition(this.selected.bounds, this.info.rotation);
		this.info.position = position;
		if (this.info.position.IsNaNOrInfinity() || this.info.rotation.IsInvalid())
			this.ResetPreview();
		this.buildablePreview.OnTargetTransformUpdate(this.info.position, this.info.rotation);
		this.UpdateValidity();
	}

	private void UpdateValidity()
	{
		bool isBlocked = this.selected.IsPlacingInsideCollider(this.buildablePreview, ref this.info);
		bool isValid = !isBlocked;
		this.info.isValid = isValid;
		if (this.info.isValid)
		{
			this.buildablePreview.GetComponent<MeshRenderer>().materials[0] = this.placementValid;
			BuildableDefinition.placementError = "No error";
		}
		else
		{
			if (isBlocked)
			{
				this.buildablePreview.GetComponent<MeshRenderer>().materials[0] = this.placementBlocked;
				BuildableDefinition.placementError = "Placing inside entity collider";
			}
			else
			{
				this.buildablePreview.GetComponent<MeshRenderer>().materials[0] = this.placementExpenses;
				BuildableDefinition.placementError = "Placing is too expensive";
			}
		}
	}

	private void ResetPreview()
	{
		if (!this.HasPreview())
			return;
		this.info.position = Vector3.zero;
		this.info.rotation = Quaternion.identity;
		this.previewDistance = 2.0f;
	}

	private bool DestroyPreview()
	{
		if (this.buildablePreview == null || !this.buildablePreview.enabled)
			return false;
		this.buildablePreview.DestroyGlobally(0.0f);
		return true;
	}

	public void ChangeSelected(BuildableDefinition definition)
	{
		if (this.selected != definition)
			this.selected = definition;
		else
			this.selected = null;
	}

	public void AskForBuild()
	{
		if (this.selected == null || this.player == null || !this.player.CanInteract())
			return;
		if (!this.info.isValid)
		{
			ClientConsole.singleton.Log("Invalid placement");
		}
		else
		{
			Network.BuildingInfo info = new Network.BuildingInfo();
			info.prefabName = this.info.prefabName;
			info.position = this.info.position;
			info.rotation = this.info.rotation.eulerAngles;
			if (this.info.point != null)
			{
				info.constructionId = this.info.point.constructionId;
				info.target = this.info.point.ownerId;
			}
			this.player.SendRpcMessage<Network.BuildingInfo>(463457298U, info, true);
		}
	}

	public void DoBuild(Network.BuildingInfo buildingInfo)
	{
		BuildingPlanner.PlacementInfo info = new BuildingPlanner.PlacementInfo();
		info.isValid = true;
		info.ray = this.player.eyes.EyesRay();
		info.player = this.player;
		info.point = BuildableDefinition.GetConstructionPoint(buildingInfo.target, buildingInfo.constructionId);
		info.position = buildingInfo.position;
		info.rotation = buildingInfo.rotation.ToQuaternion();
		info.prefabName = buildingInfo.prefabName;
		this.CreateBuildable(info, InspectorDefinition.singleton.GetDefinition<BuildableDefinition>(info.prefabName));
	}

	public BaseBuildable CreateBuildable(BuildingPlanner.PlacementInfo info, BuildableDefinition definition)
	{
		bool transformInvalid = info.player == null || info.position.IsNaNOrInfinity() || info.rotation.IsInvalid();
		bool attachInvalid = info.point != null && (info.point.isMale || info.point.owner != null);
		if (transformInvalid || attachInvalid || definition == null)
			return null;
		BaseBuildable buildable = definition.CreateBuildable(info, true);
		if (buildable == null)
			return null;
		if (info.point != null && buildable.transform.parent == null)
		{
			buildable.SetParent(info.point.owner.GetParent(), true, true);
		}
		buildable.ownerId = info.player.playerID;
		buildable.SubscribeAll();
		return buildable;
	}

	public struct PlacementInfo
	{
		public bool isValid;
		public Ray ray;
		public BasePlayer player;
		public ConstructionPoint point;
		public Vector3 position;
		public Quaternion rotation;
		public string prefabName;
	}
}