﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverlayController : MonoBehaviour
{
	public static OverlayController singleton;
	public OverlayController.Tab tab;
	public BasePlayer localPlayer;
	private Canvas overlay;
	private List<GameObject> elements;
	public bool isDrawn;
	[NonSerialized]
	public List<MonoBehaviour> focused;
	private Image filter;
	private int tabCount;
	[NonSerialized]
	public bool isOpen;

	private void OnEnable()
	{
		this.elements = new List<GameObject>(16);
		this.focused = new List<MonoBehaviour>(4);
		OverlayController.singleton = this;
		this.tabCount = typeof(Tab).GetEnumValues().Length;
		this.overlay = this.GetComponent<Canvas>();
		this.filter = this.GetComponent<Image>();
		this.filter.enabled = false;
	}

	public void SelectNextTab(bool draw = true)
	{
		OverlayController.Tab tab = this.tab + 1;
		if ((int)tab >= this.tabCount)
			tab = (OverlayController.Tab)0;
		this.SelectTab(tab, draw);
	}

	public void SelectPrevTab(bool draw = true)
	{
		OverlayController.Tab tab = this.tab - 1;
		if ((int)tab < 0)
			tab = (OverlayController.Tab)this.tabCount;
		this.SelectTab(tab, draw);
	}

	public void SelectTab(OverlayController.Tab tab, bool draw = true)
	{
		OverlayController.Tab oldTab = this.tab;
		this.tab = tab;
		if (oldTab == tab || !draw)
			return;
		this.DrawSelectedTab();
	}

	private void DrawSelectedTab()
	{
		if (this.tab == OverlayController.Tab.Inventory)
			this.DrawInventoryTab();
		else if (this.tab == OverlayController.Tab.Construction)
			this.DrawConstructionTab();
	}

	public enum Tab
	{
		Inventory,
		Construction
	}

	public bool HasFocus(MonoBehaviour focus)
	{
		return this.focused.Contains(focus);
	}

	public bool IsUsingPointer()
	{
		return this.focused.Count > 0;
	}

	private void DrawInventoryTab()
	{
		if (this.isDrawn)
			this.ClearOverlay();
		GameObject overlay = GameInstance.singleton.SpawnPrefab("assets/prefabs/player/inventory.prefab", this.overlay.transform);
		InventoryGrid grid = overlay.GetComponentInChildren<InventoryGrid>();
		if (grid == null)
		{
			Debug.LogError("Inventory overlay not found");
			return;
		}
		if (grid.inventory == null)
			grid.Init(this.localPlayer.body);
		this.elements.Add(overlay);
		this.isDrawn = true;
	}

	private void DrawConstructionTab()
	{
		if (this.isDrawn)
			this.ClearOverlay();
		GameObject overlay = GameInstance.singleton.SpawnPrefab("assets/prefabs/player/construction.prefab", this.overlay.transform);
		this.elements.Add(overlay);
		this.isDrawn = true;
	}

	private void ClearOverlay()
	{
		if (this.isDrawn)
		{
			for (int index = 0; index < this.elements.Count; index++)
			{
				GameObject element = this.elements[index];
				if (element != null)
					GameInstance.singleton.DestroyObject(element);
			}
			this.isDrawn = false;
		}
	}

	public void ClientInput()
	{
		Cursor.visible = this.IsUsingPointer();
		if (Input.GetKeyDown(KeyCode.Comma))
			this.SelectPrevTab();
		if (Input.GetKeyDown(KeyCode.Period))
			this.SelectNextTab();
	}

	public void Open()
	{
		if (this.isOpen)
			return;
		this.filter.enabled = true;
		this.focused.Add(this);
		this.DrawSelectedTab();
		this.isOpen = true;
	}

	public void Toggle()
	{
		if (this.isOpen)
			this.Close();
		else
			this.Open();
	}

	public void Close()
	{
		if (!this.isOpen)
			return;
		this.filter.enabled = false;
		this.ClearOverlay();
		this.focused.Remove(this);
		this.isOpen = false;
	}
}