﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ItemInventory
{
	public uint inventoryId;
	private Dictionary<uint, BaseItem> itemsById;
	private BaseItem[] items;
	public BaseEntity owner;
	private int capacity;
	public bool init;

	public void Init(int capacity)
	{
		if (this.init)
			return;
		Debug.Assert(this.inventoryId == 0, "Inventory was already assigned an id");
		this.inventoryId = GameInstance.singleton.AllocateId();
		this.itemsById = new Dictionary<uint, BaseItem>(capacity);
		this.items = new BaseItem[capacity];
		this.capacity = capacity;
		this.init = true;
	}

	public bool IsFull()
	{
		for (int index = 0; index < this.items.Length; index++)
		{
			if (this.items[index] == null)
				return false;
		}
		return true;
	}

	public bool IsEmpty()
	{
		for (int index = 0; index < this.items.Length; index++)
		{
			if (this.items[index] != null)
				return false;
		}
		return true;
	}

	public BaseItem GetItemById(uint itemId)
	{
		BaseItem item;
		if (this.itemsById.TryGetValue(itemId, out item))
			return item;
		return null;
	}

	public BaseItem GetItem(int index)
	{
		if (this.items[index] != null)
			return this.items[index];
		return null;
	}

	public BaseItem[] GetItems()
	{
		BaseItem[] items = new BaseItem[this.items.Length];
		for (int index = 0; index < this.items.Length; index++)
			items[index] = this.items[index];
		return items;
	}

	public bool TryMove(BaseItem item)
	{
		for (int index = 0; index < this.items.Length; index++)
		{
			if (this.TryMove(item, index))
				return true;
		}
		return false;
	}

	public bool TryMove(BaseItem item, int index)
	{
		if (this.items[index] != null)
			return false;
		if (item.inventory != null)
			item.inventory.Remove(item);
		this.itemsById.Add(item.itemId, item);
		this.items[index] = item;
		item.inventory = this;
		item.index = index;
		return true;
	}

	public int TryMerge(BaseItem item, int quantity)
	{
		int remaining = quantity;
		for (int index = 0; index < this.items.Length; index++)
		{
			remaining = remaining - this.TryMerge(item, remaining, index);
			Debug.Assert(remaining >= 0, "Moved more items than were in the stack");
			if (remaining == 0)
				return quantity;
		}
		return quantity - remaining;
	}

	public int TryMerge(BaseItem item, int quantity, int index)
	{
		BaseItem target = this.GetItem(index);
		if (target == null)
			return this.TryMove(item, index) ? quantity : 0;
		int remaining = quantity - this.MoveItems(item, target, quantity);
		if (remaining != 0)
			return quantity - remaining;
		target.inventory.Remove(item);
		target.index = 0;
		return quantity;
	}

	private int MoveItems(BaseItem source, BaseItem target, int quantity)
	{
		int moved = target.stackable - target.quantity;
		if (moved > quantity)
			moved = quantity;
		target.quantity += moved;
		source.quantity -= moved;
		return moved;
	}

	public int Add(BaseItem item)
	{
		int quantity = item.quantity;
		int remaining = quantity - this.TryMerge(item, quantity);
		if (remaining == 0)
			return quantity;
		return this.TryMove(item) ? quantity : quantity - remaining;
	}

	private bool Remove(BaseItem item)
	{
		if (item == null || item.inventory == null)
			return false;
		int index = item.index;
		if (this.GetItem(index) == null)
			return false;
		this.itemsById.Remove(item.itemId);
		this.items[index] = null;
		item.inventory = null;
		return true;
	}

	public Network.ItemInventory Save()
	{
		Network.ItemInventory inventory = new Network.ItemInventory();
		inventory.items = new Network.BaseItem[this.capacity];
		inventory.owner = this.owner.networkableId;
		inventory.inventoryId = this.inventoryId;
		inventory.capacity = this.capacity;
		for (int index = 0; index < this.items.Length; index++)
		{
			BaseItem item = this.items[index];
			if (item == null)
				continue;
			inventory.items[index] = item.Save();
		}
		return inventory;
	}

	public void Load(Network.ItemInventory inventory)
	{
		this.owner = BaseNetworkable.GetNetworkable(inventory.owner) as BaseEntity;
		this.inventoryId = inventory.inventoryId;
		this.capacity = inventory.capacity;
		this.itemsById = new Dictionary<uint, BaseItem>(this.capacity);
		this.items = new BaseItem[this.capacity];
		for (int index = 0; index < inventory.items.Length; index++)
		{
			Network.BaseItem item = inventory.items[index];
			if (item != null)
			{
				BaseItem baseItem = new BaseItem();
				baseItem.inventory = this;
				baseItem.Load(item);
				this.itemsById.Add(baseItem.itemId, baseItem);
				this.items[index] = baseItem;
			}
		}
		this.init = true;
	}
}