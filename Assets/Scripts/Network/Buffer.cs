﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

public class Serializer : IDisposable
{
	public static readonly int preallocationSize = 256;
	[ThreadStatic]
	private static Serializer preallocated;
	public Message message;
	private byte[] buffer;
	public int position;

	public Serializer(int size) : this(size, 0)
	{
	}

	public Serializer(int size, int offset)
	{
		this.buffer = new byte[size];
		this.position = offset;
	}

	public Serializer(ref byte[] buffer) : this(ref buffer, 0)
	{
	}

	public Serializer(ref byte[] buffer, int offset)
	{
		this.buffer = buffer;
		this.position = offset;
	}

	public byte[] GetArray(bool resetPosition = false)
	{
		byte[] array = new byte[position];
		for (int i = 0; i < position; i++)
			array[i] = this.buffer[i];
		if (resetPosition)
			this.position = 0;
		return array;
	}

	private void WriteLittleEndian(ulong value, int bytes)
	{
		for (int i = 0; i < bytes; i++)
			this.buffer[this.position++] = (byte)(value >> i * 8);
	}

	private ulong ReadLittleEndian(int bytes)
	{
		ulong value = 0;
		for (int i = 0; i < bytes; i++)
			value |= (ulong)this.buffer[this.position++] << i * 8;
		return value;
	}

	public void WriteType(Message.Type type)
	{
		this.WriteByte((byte)type);
	}

	public void WriteByte(byte value)
	{
		this.buffer[this.position++] = value;
	}

	public void WriteBool(bool value)
	{
		this.WriteByte((byte)(value ? 1 : 0));
	}

	public void WriteShort(short value)
	{
		this.WriteLittleEndian((ulong)value, 2);
	}

	public void WriteUshort(ushort value)
	{
		this.WriteLittleEndian(value, 2);
	}

	public void WriteInt(int value)
	{
		this.WriteLittleEndian((ulong)value, 4);
	}

	public void WriteUint(uint value)
	{
		this.WriteLittleEndian(value, 4);
	}

	public void WriteLong(long value)
	{
		this.WriteLittleEndian((ulong)value, 8);
	}

	public void WriteUlong(ulong value)
	{
		this.WriteLittleEndian(value, 8);
	}

	public void WriteFloat(float value)
	{
		Serializer.Union32Bit union = new Union32Bit();
		union.f = value;
		this.WriteLittleEndian(union.i, 4);
	}

	public void WriteDouble(double value)
	{
		Serializer.Union64Bit union = new Union64Bit();
		union.d = value;
		this.WriteLittleEndian(union.i, 8);
	}

	public void WriteVector2(Vector2 value)
	{
		this.WriteFloat(value.x);
		this.WriteFloat(value.y);
	}

	public void WriteVector3(Vector3 value)
	{
		this.WriteFloat(value.x);
		this.WriteFloat(value.y);
		this.WriteFloat(value.z);
	}

	public void WriteBytesWithLength(byte[] value)
	{
		this.WriteByte((byte)value.Length);
		for (int i = 0; i < value.Length; i++)
			this.WriteByte(value[i]);
	}

	public void WriteString(string value)
	{
		this.WriteString(value, Encoding.ASCII);
	}

	public void WriteString(string value, Encoding encoding)
	{
		this.WriteBytesWithLength(encoding.GetBytes(value));
	}

	public Message.Type ReadType()
	{
		return (Message.Type)this.ReadByte();
	}

	public byte ReadByte()
	{
		return this.buffer[this.position++];
	}

	public bool ReadBool()
	{
		return this.ReadByte() == 1;
	}

	public short ReadShort()
	{
		return (short)this.ReadLittleEndian(2);
	}

	public ushort ReadUshort()
	{
		return (ushort)this.ReadLittleEndian(2);
	}

	public int ReadInt()
	{
		return (int)this.ReadLittleEndian(4);
	}

	public uint ReadUint()
	{
		return (uint)this.ReadLittleEndian(4);
	}

	public long ReadLong()
	{
		return (long)this.ReadLittleEndian(8);
	}

	public ulong ReadUlong()
	{
		return (ulong)this.ReadLittleEndian(8);
	}

	public float ReadFloat()
	{
		Serializer.Union32Bit union = new Union32Bit();
		union.i = (uint)this.ReadLittleEndian(4);
		return union.f;
	}

	public double ReadDouble()
	{
		Serializer.Union64Bit union = new Union64Bit();
		union.i = (uint)this.ReadLittleEndian(8);
		return union.d;
	}

	public Vector2 ReadVector2()
	{
		Vector2 value;
		value.x = this.ReadFloat();
		value.y = this.ReadFloat();
		return value;
	}

	public Vector3 ReadVector3()
	{
		Vector3 value;
		value.x = this.ReadFloat();
		value.y = this.ReadFloat();
		value.z = this.ReadFloat();
		return value;
	}

	public byte[] ReadBytesWithLength()
	{
		byte[] value = new byte[this.ReadByte()];
		for (int i = 0; i < value.Length; i++)
			value[i] = this.ReadByte();
		return value;
	}

	public string ReadString()
	{
		return this.ReadString(Encoding.ASCII);
	}

	public string ReadString(Encoding encoding)
	{
		return encoding.GetString(this.ReadBytesWithLength());
	}

	[StructLayout(LayoutKind.Explicit)]
	public struct Union32Bit
	{
		[FieldOffset(0)]
		public uint i;

		[FieldOffset(0)]
		public float f;
	}

	[StructLayout(LayoutKind.Explicit)]
	public struct Union64Bit
	{
		[FieldOffset(0)]
		public ulong i;

		[FieldOffset(0)]
		public double d;
	}

	public void Dispose()
	{
	}

	public static Serializer GetPreallocated()
	{
		if (preallocated == null)
			preallocated = new Serializer(preallocationSize);
		return preallocated;
	}

	public void WriteObject<T>(T value)
	{
		Type type = typeof(T);
		if (typeof(Network.ISerialized).IsAssignableFrom(type))
			(value as Network.ISerialized).ToMessage(this.message);
		else if (type == typeof(bool))
			this.WriteBool((bool)(object)value);
		else if (type == typeof(string))
			this.WriteString((string)(object)value);
		else if (type == typeof(float))
			this.WriteFloat((float)(object)value);
		else if (type == typeof(double))
			this.WriteDouble((double)(object)value);
		else if (type == typeof(int))
			this.WriteInt((int)(object)value);
		else if (type == typeof(uint))
			this.WriteUint((uint)(object)value);
		else if (type == typeof(Vector3))
			this.WriteVector3((Vector3)(object)value);
		else if (type == typeof(long))
			this.WriteLong((long)(object)value);
		else if (type == typeof(ulong))
			this.WriteUlong((ulong)(object)value);
		else if (type == typeof(short))
			this.WriteShort((short)(object)value);
		else if (type == typeof(ushort))
			this.WriteUshort((ushort)(object)value);
		else if (type == typeof(Vector2))
			this.WriteVector2((Vector2)(object)value);
		else if (type == typeof(byte))
			this.WriteByte((byte)(object)value);
		else
			Debug.LogWarning("No generic write support for type " + type);
	}
}