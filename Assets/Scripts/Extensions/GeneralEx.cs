﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GeneralEx
{
	public static bool Invert(this bool value)
	{
		return (bool)!value;
	}
}