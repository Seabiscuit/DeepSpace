﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectEx
{
	public static void DestroyImmediate(this Object obj)
	{
		MonoBehaviour.DestroyImmediate(obj, false);
	}

	public static void DestroyGlobally(this BaseNetworkable obj, float delay = 0.0f)
	{
		GameInstance.singleton.DestroyEntity(obj.gameObject);
	}

	public static void DestroyMany(this Object[] obj, float delay = 0.0f)
	{
		for (int index = 0; index < obj.Length; index++)
		{
			MonoBehaviour.Destroy(obj[index], delay);
		}
	}
}