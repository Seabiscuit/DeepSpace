﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEyes : MonoBehaviour
{
	public readonly Vector3 firstOffset = new Vector3(0.0f, 0.425f, 0.519f);
	public readonly Vector3 thirdOffset = new Vector3(0.0f, 0.0f, 0.0f);
	private PlayerEyes.ViewMode viewMode;
	public string debugName;
	[NonSerialized]
	public Transform body;
	[NonSerialized]
	public Camera view;
	private BasePlayer player;
	public bool isServer;

	public void SetViewModeFirstPerson()
	{
		PlayerEyes.ViewMode viewMode = PlayerEyes.ViewMode.FirstPerson;
		Debug.Assert(this.viewMode != viewMode, "View mode is already first person");
		if (this.viewMode == viewMode || this.isServer || this.view == null)
			return;
		this.view.transform.SetParent(this.body);
		this.view.transform.localPosition = this.firstOffset;
		this.viewMode = viewMode;
	}

	public void InitPlayerEyes(BasePlayer player)
	{
		this.player = player;
		if (this.player == null || !this.player.enabled)
			return;
		this.debugName = this.player.debugName + "eyes";
		if (this.player.IsLocalPlayer())
			this.view = MonoBehaviour.FindObjectOfType<Camera>();
		else
			this.isServer = true;
	}

	public Quaternion BodyRotation()
	{
		return this.body.rotation;
	}

	public Vector3 BodyPosition()
	{
		return this.body.position;
	}

	public Ray BodyRay()
	{
		return new Ray(this.BodyPosition(), this.body.forward);
	}

	public Quaternion EyesRotation()
	{
		return this.transform.rotation;
	}

	public Vector3 EyesPosition()
	{
		return this.transform.position;
	}

	public Vector3 EyesForward()
	{
		return this.transform.rotation * Vector3.forward;
	}

	public Vector3 EyesUp()
	{
		return this.transform.rotation * Vector3.up;
	}

	public Vector3 EyesRight()
	{
		return this.transform.rotation * Vector3.right;
	}

	public Ray EyesRay()
	{
		return new Ray(this.EyesPosition(), this.EyesForward());
	}

	public enum ViewMode
	{
		None,
		FirstPerson
	}
}