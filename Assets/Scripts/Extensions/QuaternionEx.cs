﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class QuaternionEx
{
	public static bool IsInvalid(this Quaternion value)
	{
		return Quaternion.Dot(value, value) < float.Epsilon;
	}
}