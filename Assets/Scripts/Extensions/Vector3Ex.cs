﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Ex
{
	public static bool IsNaNOrInfinity(this Vector3 value)
	{
		return float.IsNaN(value.x) || float.IsInfinity(value.x) || float.IsNaN(value.y) || float.IsInfinity(value.y) || float.IsNaN(value.z) || float.IsInfinity(value.z);
	}

	public static Quaternion ToQuaternion(this Vector3 value)
	{
		return Quaternion.Euler(value);
	}
}