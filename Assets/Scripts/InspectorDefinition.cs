﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class InspectorDefinition : MonoBehaviour
{
	public static readonly InspectorDefinition.DefinitionCollection singleton = new InspectorDefinition.DefinitionCollection();
	[NonSerialized]
	public Vector3 localPosition;
	[NonSerialized]
	public Quaternion localRotation;
	[NonSerialized]
	public string prefabName;
	[NonSerialized]
	public string debugName;

	public virtual void Load(string name)
	{
		this.prefabName = name;
		this.debugName = this.GetType().Name;
		this.localPosition = this.transform.localPosition;
		this.localRotation = this.transform.localRotation;
		InspectorDefinition.singleton.AddDefinition(this.prefabName, this);
	}

	public override string ToString()
	{
		if (string.IsNullOrEmpty(this.debugName))
			return string.Empty;
		return this.debugName;
	}

	public class DefinitionCollection
	{
		private Dictionary<string, List<InspectorDefinition>> definitionsByName = new Dictionary<string, List<InspectorDefinition>>();

		public TDefinition GetDefinition<TDefinition>(string prefabName) where TDefinition : InspectorDefinition
		{
			return this.GetDefinitions(prefabName).OfType<TDefinition>().FirstOrDefault() as TDefinition;
		}

		public List<InspectorDefinition> GetDefinitions(string prefabName)
		{
			List<InspectorDefinition> definitions;
			if (this.definitionsByName.TryGetValue(prefabName, out definitions))
				return definitions;
			List<InspectorDefinition> definitions2 = new List<InspectorDefinition>();
			this.definitionsByName.Add(prefabName, definitions2);
			return definitions2;
		}

		public void AddDefinition(string prefabName, InspectorDefinition definition)
		{
			List<InspectorDefinition> definitions = this.GetDefinitions(prefabName);
			if (definitions.Contains(definition))
				return;
			definitions.Add(definition);
		}
	}
}