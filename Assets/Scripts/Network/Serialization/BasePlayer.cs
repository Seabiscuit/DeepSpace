﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
	public class BasePlayer : ISerialized
	{
		public ItemInventory hotbar;
		public ItemInventory body;
		public ulong clientId;
		public string name;

		public void FromMessage(Message message)
		{
			this.clientId = message.buffer.ReadUlong();
			this.name = message.buffer.ReadString();
			if (message.buffer.ReadBool())
			{
				this.hotbar = new Network.ItemInventory();
				this.hotbar.FromMessage(message);
			}
			if (message.buffer.ReadBool())
			{
				this.body = new Network.ItemInventory();
				this.body.FromMessage(message);
			}
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteUlong(this.clientId);
			message.buffer.WriteString(this.name);
			message.buffer.WriteBool(this.hotbar != null);
			if (this.hotbar != null)
			{
				this.hotbar.ToMessage(message);
			}
			message.buffer.WriteBool(this.body != null);
			if (this.body != null)
			{
				this.body.ToMessage(message);
			}
		}
	}
}