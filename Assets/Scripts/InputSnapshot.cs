﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class InputSnapshot
{
	private Vector3 mouseDelta = Vector3.zero;
	private int currentBindings = 0;
	private int lastBindings = 0;
	private float timeDelta;

	public Vector3 GetMouseDelta()
	{
		return this.mouseDelta;
	}

	public bool WasJustPressed(InputBinding binding)
	{
		if (this.IsDown(binding))
			return !this.WasDown(binding);
		return false;
	}

	public bool WasJustReleased(InputBinding binding)
	{
		if (!this.IsDown(binding))
			return this.WasDown(binding);
		return false;
	}

	public bool IsDown(InputBinding binding)
	{
		if (this.currentBindings == 0)
			return false;
		return (this.currentBindings & (int)binding) == (int)binding;
	}

	public bool WasDown(InputBinding binding)
	{
		if (this.lastBindings == 0)
			return false;
		return (this.lastBindings & (int)binding) == (int)binding;
	}

	public void Capture(InputSnapshot lastSnapshot)
	{
		this.CaptureBinding(InputBinding.Forward);
		this.CaptureBinding(InputBinding.Backward);
		this.CaptureBinding(InputBinding.StrafeRight);
		this.CaptureBinding(InputBinding.StrafeLeft);
		this.CaptureBinding(InputBinding.Up);
		this.CaptureBinding(InputBinding.Down);
		this.CaptureBinding(InputBinding.RollRight);
		this.CaptureBinding(InputBinding.RollLeft);
		this.CaptureBinding(InputBinding.Dampeners);
		this.CaptureBinding(InputBinding.BuildMode);
		this.CaptureBinding(InputBinding.Overlay);
		this.mouseDelta.x = UnityEngine.Input.GetAxis("Mouse X");
		this.mouseDelta.y = UnityEngine.Input.GetAxis("Mouse Y");
		this.mouseDelta.z = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
		if (lastSnapshot != null)
			this.lastBindings = lastSnapshot.currentBindings;
		else
			this.lastBindings = 0;
		this.timeDelta = Time.deltaTime;
	}

	private void CaptureBinding(InputBinding binding)
	{
		if (!UnityEngine.Input.GetKey(InputMappings.keyMappings[binding]))
			return;
		this.currentBindings |= (int)binding;
	}

	public void Reset()
	{
		this.currentBindings = 0;
		this.lastBindings = 0;
		this.timeDelta = 0.0f;
	}
}