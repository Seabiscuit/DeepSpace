﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class BaseNetworkable : MonoBehaviour
{
	private static Dictionary<uint, BaseNetworkable> networkablesById = new Dictionary<uint, BaseNetworkable>();
	[NonSerialized]
	public uint networkableId;
	public string prefabName;
	private List<Connection> subscribers;
	public Peer peer;
	public string debugName;
	public bool isStatic;
	private bool globalBroadcast;
	[NonSerialized]
	public ushort prefabId;
	private bool isDestroyed;

	public static BaseNetworkable GetNetworkable(uint id)
	{
		BaseNetworkable networkable;
		if (BaseNetworkable.networkablesById == null)
			return null;
		networkablesById.TryGetValue(id, out networkable);
		return networkable;
	}

	public static List<BaseNetworkable> GetNetworkables()
	{
		if (BaseNetworkable.networkablesById != null)
			return networkablesById.Values.ToList();
		return null;
	}

	public Vector3 GetNetworkPosition()
	{
		if (this.peer.IsServer())
			return this.transform.position;
		return Vector3.zero;
	}

	public Quaternion GetNetworkRotation()
	{
		if (this.peer.IsServer())
			return this.transform.rotation;
		return Quaternion.identity;
	}

	public string GetDebugName()
	{
		return string.IsNullOrEmpty(this.debugName) ? this.name : this.debugName;
	}

	public bool IsLocalPlayer()
	{
		if (this.peer.IsServer() || BasePlayer.localPlayer == null)
			return false;
		return this.networkableId == BasePlayer.localPlayer.networkableId;
	}

	public virtual bool IsPlayer(ulong clientId)
	{
		return false;
	}

	public bool IsNetworked()
	{
		return this.networkableId != 0 && !this.isDestroyed;
	}

	protected virtual void OnNetworkInit()
	{
	}

	protected virtual void OnNetworkDestroy()
	{
	}

	protected virtual void OnLocalInit()
	{
	}

	protected virtual void OnLocalDestroy()
	{
	}

	public void NetworkInit()
	{
		Debug.Assert(this.peer != null, "Peer was null");
		if (this.peer == null || this.subscribers != null)
			return;
		this.subscribers = new List<Connection>();
		Debug.Assert(this.peer.IsServer(), "Networkable init should not be called client side");
		this.networkableId = GameInstance.singleton.AllocateId();
		this.LocalInit();
		this.OnNetworkInit();
	}

	public void LocalInit()
	{
		if (this.networkableId == 0U)
			return;
		this.isDestroyed = false;
		BaseNetworkable.networkablesById.Add(this.networkableId, this);
		this.OnLocalInit();
		Debug.LogFormat("Networkable {0} ({1}) initialized", this.networkableId, this.debugName);
	}

	public void NetworkDestroy()
	{
		if (this.peer == null || this.networkableId == 0U)
			return;
		Debug.Assert(this.peer.IsServer(), "Networkable must be destroyed server side");
		if (this.isDestroyed)
			return;
		this.BroadcastStateToSubscribers(false, true);
		this.ClearSubscribers();
		this.subscribers = null;
		this.OnNetworkDestroy();
		this.LocalDestroy();
	}

	public void LocalDestroy()
	{
		if (this.networkableId == 0U || this.isDestroyed)
			return;
		this.OnLocalDestroy();
		uint id = this.networkableId;
		this.networkableId = 0U;
		BaseNetworkable.networkablesById.Remove(id);
		this.isDestroyed = true;
		Debug.LogFormat("Networkable {0} ({1}) destroyed", id, this.debugName);
	}

	public bool IsSubscriber(Connection connection)
	{
		if (this.subscribers == null)
			return false;
		return this.subscribers.Contains(connection);
	}

	public List<Connection> GetSubscribers()
	{
		if (this.subscribers != null)
			return this.globalBroadcast ? this.peer.GetConnections() : this.subscribers;
		else if (this.globalBroadcast && this.peer != null)
			return this.peer.GetConnections();
		return null;
	}

	public void SubscribeAll(bool broadcast = true)
	{
		List<Connection> connections = this.peer.GetConnections();
		if (connections != null)
		{
			this.SubscribeMany(connections, broadcast);
		}
	}

	public void SubscribeAllButOne(Connection connection, bool broadcast = true)
	{
		List<Connection> connections = this.peer.GetConnections();
		if (connections != null)
		{
			connections.Remove(connection);
			this.SubscribeMany(connections, broadcast);
		}
	}

	public void SubscribeMany(List<Connection> connections, bool broadcast = true)
	{
		if (this.subscribers == null)
			return;
		foreach (Connection connection in connections)
		{
			if (this.subscribers.Contains(connection))
				continue;
			this.subscribers.Add(connection);
			if (broadcast)
				this.BroadcastStateToConnection(connection, true, false);
		}
	}

	public void AddSubscriber(Connection connection, bool broadcast = true)
	{
		if (this.subscribers == null || connection == null)
			return;
		if (this.subscribers.Contains(connection))
			Debug.LogWarning("Connection is already subscribed to this networkable");
		else
		{
			this.subscribers.Add(connection);
			if (broadcast)
				this.BroadcastStateToConnection(connection, true, false);
		}
	}

	public void RemoveSubscriber(Connection connection, bool broadcast = true)
	{
		if (this.subscribers == null || !this.subscribers.Contains(connection))
			return;
		this.subscribers.Remove(connection);
		if (broadcast)
			this.BroadcastStateToConnection(connection, false, true);
	}

	public void ClearSubscribers()
	{
		if (this.subscribers == null)
			return;
		this.subscribers.Clear();
	}

	public void SendMessageToSubscribers(Message message, bool isReliable)
	{
		List<Connection> connections = new List<Connection>();
		if (this.peer.IsServer())
			connections.AddRange(this.GetSubscribers());
		else
			connections.Add(this.peer.GetClient());
		this.peer.SendMessage(message, connections, isReliable);
	}

	public void BroadcastStateToConnection(Connection connection, bool create, bool destroy)
	{
		if (this.peer == null || create == destroy)
			return;
		Message message = new Message();
		message.buffer.WriteType(create ? Message.Type.Create : Message.Type.Destroy);
		message.buffer.WriteUint(this.networkableId);
		bool isLocalPlayer = this.IsPlayer(connection.networkId);
		if (create)
		{
			message.buffer.WriteBool(isLocalPlayer);
			if (!isLocalPlayer)
				message.buffer.WriteString(this.prefabName);
			BaseNetworkable.SaveState state = new BaseNetworkable.SaveState();
			this.Save(ref state);
			if (state.networkable == null)
				UnityEngine.Debug.LogError("Saving networkable to message failed");
			state.networkable.Serialize(message);
			//Debug.LogFormat("Broadcasting create for networkable {0} to {1}", this.networkableId, connection.networkId);
		}
		this.peer.SendMessage(message, connection, true);
	}

	public void BroadcastStateToSubscribers(bool create, bool destroy)
	{
		if (this.peer == null || create == destroy)
			return;
		Debug.Assert(this.peer.IsServer(), "Only server should perform broadcasting");
		List<Connection> subscribers = this.GetSubscribers();
		if (subscribers == null || subscribers.Count == 0)
			return;
		foreach (Connection subscriber in subscribers)
		{
			this.BroadcastStateToConnection(subscriber, create, destroy);
		}
	}

	public void BroadcastStateToAll(bool create, bool destroy)
	{
		if (this.peer == null || create == destroy)
			return;
		Debug.Assert(this.peer.IsServer(), "Only server should perform broadcasting");
		List<Connection> connections = this.peer.GetConnections();
		if (connections != null)
		{
			if (connections.Count > 0)
			{
				foreach (Connection connection in connections)
					this.BroadcastStateToConnection(connection, create, destroy);
			}
		}
	}

	public virtual void Save(ref BaseNetworkable.SaveState state)
	{
		UnityEngine.Debug.Assert(this.peer.IsServer(), "Saving should not be called client side");
		if (this.networkableId == 0U)
			UnityEngine.Debug.LogError("Networkable lacks identity");
		state.networkable = new Network.Networkable();
		state.networkable.networkableId = this.networkableId;
		state.networkable.prefabName = this.prefabName;
	}

	public virtual void Load(ref BaseNetworkable.SaveState state)
	{
		if (state.networkable == null)
			return;
		Network.Networkable networkable = state.networkable;
		this.networkableId = networkable.networkableId;
		this.prefabName = networkable.prefabName;
		if (this.networkableId == 0U)
			UnityEngine.Debug.LogError("Networkable lacks identity");
	}

	public struct SaveState
	{
		public Network.Networkable networkable;
	}
}