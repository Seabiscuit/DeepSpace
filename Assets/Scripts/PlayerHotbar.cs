﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHotbar
{
	public ItemInventory inventory = new ItemInventory();
	private readonly BasePlayer player;
	private int selectedIndex = -1;

	public PlayerHotbar(BasePlayer player)
	{
		this.player = player;
	}

	private bool IsIndexInRange(int index)
	{
		return index >= 0 && index < 8;
	}

	public BaseItem GetItemAtIndex(int index)
	{
		if (!this.inventory.init || !this.IsIndexInRange(index))
			return null;
		return this.inventory.GetItem(index);
	}

	public BaseItem GetSelectedItem()
	{
		int index = this.selectedIndex;
		if (this.inventory.init || this.IsIndexInRange(index))
			return null;
		return this.inventory.GetItem(index);
	}

	public bool HasSelection()
	{
		return this.selectedIndex >= 0;
	}

	private void SelectNextIndex()
	{
		int index = this.selectedIndex + 1;
		if (index >= 8)
			index = 0;
		this.SelectIndex(index);
	}

	private void SelectPrevIndex()
	{
		int index = this.selectedIndex - 1;
		if (index < 0)
			index = 7;
		this.SelectIndex(index);
	}

	private void SelectIndex(int index)
	{
		if (!this.inventory.init || !this.IsIndexInRange(index))
			return;
		BaseItem selectedItem = this.GetSelectedItem();
		BaseItem itemAtIndex = this.GetItemAtIndex(index);
		uint inventoryId = this.inventory.inventoryId;
		if (selectedItem != null && selectedItem is BaseTool)
		{

		}
		if (itemAtIndex != null && itemAtIndex is BaseTool)
		{

		}
		if (this.IsIndexInRange(this.selectedIndex))
		{
			InventoryGrid.ChangeSelected(inventoryId, this.selectedIndex, false);
		}
		if (this.selectedIndex != index)
		{
			InventoryGrid.ChangeSelected(inventoryId, index, true);
		}
		this.selectedIndex = index;
	}

	public void ClientInput(InputSnapshot snapshot)
	{
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha1))
			this.SelectIndex(0);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha2))
			this.SelectIndex(1);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha3))
			this.SelectIndex(2);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha4))
			this.SelectIndex(3);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha5))
			this.SelectIndex(4);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha6))
			this.SelectIndex(5);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha7))
			this.SelectIndex(6);
		if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha8))
			this.SelectIndex(7);
	}
}