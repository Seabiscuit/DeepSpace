﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ClientConsole : MonoBehaviour
{
	public static ClientConsole singleton;
	public ClientConsole.WriteMode writeMode;
	public Color textColor;
	public int maxLines;
	public int fontSize;
	private Text ui;
	private StringBuilder builder;
	private bool textChanged;
	private Queue<string> text;
	private bool isVisible;

	private void Awake()
	{
		ClientConsole.singleton = this;
		this.ui = this.GetComponentInChildren<Text>();
		Debug.Assert(this.ui != null, "No console text component");
		this.ui.fontSize = this.fontSize;
		this.builder = new StringBuilder();
		this.text = new Queue<string>(this.maxLines);
		this.isVisible = true;
		this.Hide();
	}

	public void Log(object format, params object[] args)
	{
		this.Log(string.Format(format.ToString(), args));
	}

	public void Log(string text)
	{
		if (this.text.Count >= this.maxLines)
			this.text.Dequeue();
		string time = DateTime.Now.ToString("[HH.mm.ss.fff]");
		this.text.Enqueue(time + "  " + text);
		this.textChanged = true;
	}

	public void Refresh()
	{
		if (this.textChanged)
			this.OnTextChanged();
		this.ui.text = this.builder.ToString();
	}

	private void OnTextChanged()
	{
		this.builder.Clear();
		this.builder.AppendLine();
		foreach (string str in this.text)
			this.builder.AppendLine(str);
		this.textChanged = false;
	}

	public enum WriteMode
	{

	}

	public void UpdateLocalPlayer()
	{
		if (Input.GetKeyDown(KeyCode.BackQuote))
		{
			if (this.isVisible)
				this.Hide();
			else
				this.Show();
		}
		if (this.isVisible)
		{
			this.Refresh();
		}
	}

	public void Show()
	{
		if (this.isVisible)
			return;
		OverlayController.singleton.focused.Add(this);
		this.gameObject.SetActive(true);
		this.enabled = true;
		this.isVisible = true;
	}

	public void Hide()
	{
		if (!this.isVisible)
			return;
		OverlayController.singleton.focused.Remove(this);
		this.gameObject.SetActive(false);
		this.enabled = false;
		this.isVisible = false;
	}

	public void Clear()
	{
		this.text.Clear();
		this.Refresh();
	}
}