﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryGrid : MonoBehaviour
{
	public static Dictionary<uint, InventoryGrid> gridsById = new Dictionary<uint, InventoryGrid>();
	public Dictionary<int, ItemSlot> indexToSlots;
	private ItemSlot[] slots;
	public string debugName;
	public ItemInventory inventory;
	[NonSerialized]
	public uint inventoryId;

	public ItemSlot GetSlot(int index)
	{
		ItemSlot slot;
		if (this.indexToSlots.TryGetValue(index, out slot))
			return slot;
		return null;
	}

	public static ItemSlot GetSlot(uint inventoryId, int index)
	{
		InventoryGrid grid;
		ItemSlot slot;
		if (InventoryGrid.gridsById.TryGetValue(inventoryId, out grid) && grid.indexToSlots.TryGetValue(index, out slot))
			return slot;
		return null;
	}

	public static InventoryGrid GetGrid(uint inventoryId)
	{
		InventoryGrid grid;
		if (InventoryGrid.gridsById.TryGetValue(inventoryId, out grid))
			return grid;
		return null;
	}

	public void Init(ItemInventory inventory)
	{
		Debug.Assert(inventory != null, "Inventory was null");
		this.inventory = inventory;
		this.inventoryId = inventory.inventoryId;
		this.slots = this.GetComponentsInChildren<ItemSlot>();
		if (this.slots == null)
			return;
		this.indexToSlots = new Dictionary<int, ItemSlot>(this.slots.Length);
		Debug.Assert(this.slots.Length > 0, debugName + "  has no item slots");
		if (this.slots.Length > 0)
		{
			InventoryGrid.gridsById.Add(this.inventoryId, this);
			foreach (ItemSlot slot in this.slots)
			{
				this.indexToSlots.Add(slot.index, slot);
				slot.inventory = this.inventory;
			}
		}
	}

	public static void ChangeSelected(uint inventoryId, int index, bool selected)
	{
		ItemSlot slot = InventoryGrid.GetSlot(inventoryId, index);
		Debug.Assert(slot != null, "Slot was null");
		slot.ChangeSelected(selected);
	}

	private void OnDisable()
	{
		this.inventory = null;
		InventoryGrid.gridsById.Remove(this.inventoryId);
		this.slots = null;
		this.indexToSlots = null;
	}

	private void Update()
	{
		if (this.inventory != null)
		{
			Debug.Assert(this.inventoryId > 0, debugName + " is not initialized");
			ItemSlot[] slots = this.indexToSlots.Values.ToArray();
			foreach (ItemSlot slot in slots)
				this.UpdateSlot(slot);
		}
	}

	public void UpdateSlot(ItemSlot slot)
	{
		if (slot != null)
		{
			BaseItem item = this.inventory.GetItem(slot.index);
			slot.UpdateItemSlot(item);
		}
	}

	public void UpdateSlot(BaseItem item)
	{
		if (item == null)
			return;
		ItemSlot slot = this.GetSlot(item.index);
		slot.UpdateItemSlot(item);
	}
}