﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScaleEffector : GeometricEditor.BaseEffector
{
	public float scale;
}

public class ScaleAttribute : GeometricEditor.BaseAttribute
{
	public readonly ScaleEffector effector = new ScaleEffector();

	public override float GetMinimum()
	{
		return 0.0f;
	}

	public override float GetMaximum()
	{
		return 10.0f;
	}

	public override GeometricEditor.BaseEffector GetEffector()
	{
		return this.effector;
	}
}