﻿using LiteNetLib;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public abstract class Peer : INetEventListener
{
	protected Dictionary<NetPeer, Connection> connectionsByPeer;
	public static Peer singleton;
	public ulong networkId;
	protected NetManager network;
	protected bool isServer;

	public void Init()
	{
		this.network = new NetManager(this);
		this.connectionsByPeer = new Dictionary<NetPeer, Connection>();
		singleton = this;
	}

	public virtual Connection GetClient()
	{
		return null;
	}

	public List<Connection> GetConnections()
	{
		if (this.connectionsByPeer == null)
			return null;
		return this.connectionsByPeer.Values.ToList();
	}

	public bool IsClient()
	{
		return !this.isServer;
	}

	public bool IsServer()
	{
		return this.isServer;
	}

	public void Update()
	{
		if (this.network == null)
			return;
		this.network.PollEvents();
	}

	public virtual void SendMessage(Message message, Connection connection, bool isReliable)
	{
		if (this.network == null)
			return;
		connection.peer.Send(message.buffer.GetArray(true), isReliable ? DeliveryMethod.ReliableOrdered : DeliveryMethod.Unreliable);
	}

	public virtual void SendMessage(Message message, IEnumerable<Connection> connections, bool isReliable)
	{
		if (this.network == null)
			return;
		byte[] data = message.buffer.GetArray(true);
		foreach (Connection connection in connections)
			connection.peer.Send(data, isReliable ? DeliveryMethod.ReliableOrdered : DeliveryMethod.Unreliable);
	}

	public virtual void OnMessageReceived(Message message, Connection sender)
	{
	}

	public virtual void OnPeerConnected(Connection connection)
	{
	}

	public virtual void OnPeerDisconnect(Connection connection, string reason)
	{
	}

	public void OnConnectionRequest(ConnectionRequest request)
	{
		request.Accept();
	}

	public void OnPeerConnected(NetPeer peer)
	{
		Connection connection = new Connection(peer);
		this.connectionsByPeer.Add(peer, connection);
		this.OnPeerConnected(connection);
	}

	public void OnPeerDisconnected(NetPeer peer, DisconnectInfo info)
	{
		Connection connection = this.connectionsByPeer[peer];
		this.connectionsByPeer.Remove(peer);
		this.OnPeerDisconnect(connection, info.Reason.ToString());
	}

	public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod method)
	{
		Message message = new Message();
		byte[] buffer = reader.GetRemainingBytes();
		message.buffer = new Serializer(ref buffer, 0);
		message.type = (Message.Type)message.buffer.ReadByte();
		message.length = buffer.Length;
		Connection connection = this.connectionsByPeer[peer];
		this.OnMessageReceived(message, connection);
	}

	public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
	{
	}

	public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
	{
	}

	public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
	{
	}
}