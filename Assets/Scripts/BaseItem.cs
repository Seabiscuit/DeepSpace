﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseItem
{
	public uint itemId;
	public int stackable = 10;
	public int quantity;
	public ItemInventory inventory;
	public int index;

	public bool IsFull()
	{
		return this.quantity >= this.stackable;
	}

	public bool IsEmpty()
	{
		return this.quantity == 0;
	}

	public virtual Network.BaseItem Save()
	{
		Network.BaseItem item = new Network.BaseItem();
		item.itemId = this.itemId;
		item.stackable = this.stackable;
		item.quantity = this.quantity;
		return item;
	}

	public virtual void Load(Network.BaseItem item)
	{
		this.itemId = item.itemId;
		this.stackable = item.stackable;
		this.quantity = item.quantity;
	}
}