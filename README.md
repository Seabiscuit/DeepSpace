# DeepSpace

DeepSpace is an upcoming first-person MMO sci-fi game set in the era of a new futuristic space race
and centered on large scale player interaction in a single instance procedural galaxy. Drawing
inspiration from games such as Rust, SpaceEngineers and Star Citizen, players will be able to construct
mobile and static objects, engage in combat with players and NPCs and play passively, peacefully or
aggressively with their friends, clans, and factions.

## About Us

Our developers are a small and independent collection of like-minded people who have many years of
experience in playing games just like the one we're developing. Fueled by frustration with other teams
who have failed to or have lacked the ability to deliver something of this magnitude, we were inspired
to create such a project.

## Planned Features

- Newtonian physics for all kinematic game objects including full orbital mechanics.
- A fully persistent universe. Physics sleeps for no-one.
- Fully modular construction system for absolute building freedom and customization.
- Immersive piloting experience and ship/station control system.
- Comprehensive combat mechanics including subsystem targeting, boarding, and electronic warfare.
- Realistic channel based communications system.
- Automation through a highly customizable "Protocol" system. 
- Environmental hazards (decompression, hypothermia, asphyxiation, starvation).