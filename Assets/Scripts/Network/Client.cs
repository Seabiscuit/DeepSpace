﻿using LiteNetLib;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client : Peer
{
	public IClientCallback callback;
	public string connectedAddress;
	public int connectedPort;
	public ulong clientId;
	public string serverName;
	internal bool hasAuth;
	public Connection connection;
	private NetPeer peer;
	public ushort serverId;
	public bool discovery;

	public bool Connect(string host, ulong clientId)
	{
		this.isServer = false;
		this.network.Start();
		this.peer = this.network.Connect(host, 7575, "");
		if (this.peer == null)
			return false;
		this.connectedAddress = host;
		this.connectedPort = 7575;
		this.networkId = this.clientId = clientId;
		this.serverName = "Debug";
		this.hasAuth = false;
		return true;
	}

	public override Connection GetClient()
	{
		return this.connection;
	}

	public bool IsConnected()
	{
		return this.peer != null;
	}

	public override void OnMessageReceived(Message message, Connection sender)
	{
		if (this.callback != null)
			this.callback.OnMessageReceived(message, sender);
	}

	public override void OnPeerConnected(Connection connection)
	{
		this.connection = connection;
		this.connection.networkId = this.clientId;
		if (this.connection != null)
			this.callback.OnClientConnected(this.connectedAddress, this.connectedPort);
	}

	public override void OnPeerDisconnect(Connection connection, string reason)
	{
		this.connection = connection;
		if (this.connection != null)
			this.callback.OnClientDisconnected(reason);
	}

	public bool HasAuthority()
	{
		return this.hasAuth;
	}

	public void ClearDiscovery()
	{
		if (this.network != null)
			return;
		this.serverName = null;
		this.discovery = false;
		this.serverId = 0;
	}

	public void Disconnect(string reason, bool sendReasonToServer)
	{
		if (this.network == null || this.peer == null)
			return;
		if (sendReasonToServer && !string.IsNullOrEmpty(reason))
		{
			Message message = new Message();
			message.buffer.WriteType(Message.Type.Close);
			message.buffer.WriteString(reason);
			this.SendMessage(message, this.connection, true);
		}
		this.peer.Disconnect();
		this.peer = null;
		this.network.Stop();
		this.network = null;
		this.ClearDiscovery();
	}
}

public interface IClientCallback
{
	void OnClientConnected(string connectedAddress, int connectedPort);
	void OnClientDisconnected(string reason);

	void OnMessageReceived(Message message, Connection sender);
}