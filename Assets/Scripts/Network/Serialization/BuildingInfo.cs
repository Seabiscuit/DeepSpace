﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
	public class BuildingInfo : ISerialized
	{
		public Vector3 position;
		public Vector3 rotation;
		public uint constructionId;
		public uint target;
		public string prefabName = "";

		public void FromMessage(Message message)
		{
			this.position = message.buffer.ReadVector3();
			this.rotation = message.buffer.ReadVector3();
			this.prefabName = message.buffer.ReadString();
		}

		public void ToMessage(Message message)
		{
			message.buffer.WriteVector3(this.position);
			message.buffer.WriteVector3(this.rotation);
			message.buffer.WriteString(this.prefabName);
		}
	}
}