﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class GameClient : MonoBehaviour, IClientCallback
{
	public static GameClient singleton;
	private Dictionary<Message.Type, Action<Message>> handlers = new Dictionary<Message.Type, Action<Message>>();
	protected HashSet<ulong> subscriptions = new HashSet<ulong>();
	private bool registered;
	[NonSerialized]
	public Canvas overlay;
	public string ipAddress = "localhost";
	private Client client;
	private ulong clientId;

	public bool IsActive { get; protected set; }

	private void Update()
	{
		if (this.client == null || this.client.IsConnected())
			this.client.Update();
		this.UpdateLocalPlayer();
	}

	public void OnClientConnected(string connectedAddress, int connectedPort)
	{
	}

	public void OnClientDisconnected(string reason)
	{
	}

	public void Connect(string host, ulong clientId)
	{
		Debug.Assert(this.registered, "Message handlers unregistered");
		if (!this.registered || (this.client != null && this.client.IsConnected()))
			return;
		this.client = new Client();
		this.client.callback = this;
		this.client.Init();
		if (this.client.Connect(host, clientId))
			Debug.Log("Client connected");
		else
			Debug.LogError("Client connect failed");
	}

	public void Disconnect(string reason, bool sendReasonToServer)
	{
		if (this.client == null || !this.client.IsConnected())
			return;
		this.client.Disconnect(reason, sendReasonToServer);
	}

	private void UpdateLocalPlayer()
	{
		ClientConsole.singleton.UpdateLocalPlayer();
		BasePlayer localPlayer = BasePlayer.localPlayer;
		if (localPlayer == null)
			return;
		localPlayer.UpdateLocalPlayer();
	}

	private void OnEnable()
	{
		InputMappings.LoadKeymap();
		this.clientId = (ulong)DateTime.Now.Ticks;
		this.overlay = MonoBehaviour.FindObjectOfType<Canvas>();
		GameInstance.singleton.SpawnPrefab("assets/prefabs/player/console.prefab", this.overlay.transform);
		MethodInfo[] methods = typeof(GameClient).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic);
		foreach (MethodInfo method in methods)
		{
			object attribute = method.GetCustomAttributes(typeof(Message.Handler), false).FirstOrDefault();
			if (attribute == null)
				continue;
			Message.Handler descriptor = attribute as Message.Handler;
			Action<Message> action = Delegate.CreateDelegate(typeof(Action<Message>), this, method) as Action<Message>;
			this.handlers.Add(descriptor.type, action);
		}
		BaseEntity.RegisterRpcTargets();
		this.registered = true;
		GameClient.singleton = this;
		this.Connect(this.ipAddress, this.clientId);
	}

	private void OnApplicationQuit()
	{
		this.overlay = this.GetComponentInChildren<Canvas>();
		this.Disconnect(null, false);
	}

	[Message.Handler(Message.Type.Discovery)]
	private void NetworkDiscovery(Message message)
	{
		ushort serverId = message.buffer.ReadUshort();
		this.client.serverId = serverId;
		this.client.serverName = message.buffer.ReadString();
		this.client.discovery = true;
		Message auth = new Message();
		auth.buffer.WriteByte((byte)Message.Type.Auth);
		auth.buffer.WriteUlong(this.client.clientId);
		this.client.SendMessage(auth, this.client.connection, true);
	}

	[Message.Handler(Message.Type.Auth)]
	private void NetworkAuth(Message message)
	{
		bool hasAuth = this.client.hasAuth = message.buffer.ReadBool();
		if (!hasAuth)
			Debug.LogWarning("Client failed auth");
		else
		{
			this.client.connection.isAuthenicated = true;
			this.client.connection.IsReady = true;
			Message ready = new Message();
			ready.buffer.WriteType(Message.Type.Ready);
			this.client.SendMessage(ready, this.client.connection, true);
		}
	}

	public void OnMessageReceived(Message message, Connection sender)
	{
		Action<Message> action;
		if (this.handlers.TryGetValue(message.type, out action))
			action.Invoke(message);
	}

	[Message.Handler(Message.Type.Create)]
	private void NetworkCreate(Message message)
	{
		uint networkableId = message.buffer.ReadUint();
		if (networkableId == 0U)
			return;
		bool isLocalPlayer = message.buffer.ReadBool();
		BaseEntity entity = BaseNetworkable.GetNetworkable(networkableId) as BaseEntity;
		Debug.AssertFormat(entity == null, "Entity {0} already exists locally", networkableId);
		if (entity == null)
		{
			if (isLocalPlayer)
				entity = GameInstance.singleton.CreateEntity("assets/prefabs/entity/player.prefab");
			else
				entity = GameInstance.singleton.CreateEntity(message.buffer.ReadString());
			Debug.Assert(entity != null, "Failed to create network entity");
			entity.networkableId = networkableId;
			entity.LocalInit();
			BaseNetworkable.SaveState state = new BaseNetworkable.SaveState();
			state.networkable = new Network.Networkable();
			state.networkable.Deserialize(message);
			entity.Load(ref state);
			if (isLocalPlayer)
				this.OnLocalPlayerCreate((BasePlayer)entity);
			this.subscriptions.Add(networkableId);
			//Debug.LogFormat("Broadcasting recieved for networkable {0}", networkableId);
		}
	}

	private void OnLocalPlayerCreate(BasePlayer player)
	{
		player.connection = this.client.connection;
		if (Camera.allCamerasCount > 0)
			player.InitLocalPlayer();
		else
			Debug.LogError("No camera available");
	}

	[Message.Handler(Message.Type.Destroy)]
	private void NetworkDestroy(Message message)
	{
		uint networkableId = message.buffer.ReadUint();
		if (networkableId == 0U)
			return;
		BaseEntity entity = BaseNetworkable.GetNetworkable(networkableId) as BaseEntity;
		Debug.AssertFormat(entity != null, "Entity {0} doesn't exist locally", networkableId);
		if (entity != null)
		{
			GameInstance.singleton.DestroyEntity(entity.gameObject);
			this.subscriptions.Remove(networkableId);
		}
	}

	[Message.Handler(Message.Type.Transform)]
	private void NetworkTransform(Message message)
	{
		uint networkableId = message.buffer.ReadUint();
		BaseEntity entity = BaseNetworkable.GetNetworkable(networkableId) as BaseEntity;
		if (entity == null)
		{
			Debug.LogErrorFormat("Received a transform for an entity ({0}) not known locally", networkableId);
			return;
		}
		entity.OnTargetTransformUpdate(message.buffer.ReadVector3(), Quaternion.Euler(message.buffer.ReadVector3()));
	}

	[Message.Handler(Message.Type.Update)]
	private void NetworkPlayerUpdate(Message message)
	{
		Network.PlayerUpdate update = new Network.PlayerUpdate();
		update.FromMessage(message);
		uint playerId = message.buffer.ReadUint();
		BasePlayer player = BaseNetworkable.GetNetworkable(playerId) as BasePlayer;
		if (player == null)
			return;
		player.OnTargetTransformUpdate(update.bodyPosition, update.bodyRotation);
	}

	[Message.Handler(Message.Type.Rpc)]
	private void NetworkRpcMessage(Message message)
	{
		uint networkableId = message.buffer.ReadUint();
		if (networkableId == 0U)
			return;
		uint rpcId = message.buffer.ReadUint();
		MethodInfo rpc;
		if (BaseEntity.rpcTargets.TryGetValue(rpcId, out rpc))
		{
			BaseEntity entity = BaseNetworkable.GetNetworkable(networkableId) as BaseEntity;
			if (entity != null)
			{
				rpc.Invoke(entity, new object[] { null, message });
			}
		}
	}

	[Message.Handler(Message.Type.Close)]
	private void NetworkClose(Message message)
	{
		string reason = message.buffer.ReadString();
		this.OnClientDisconnected(reason);
	}
}