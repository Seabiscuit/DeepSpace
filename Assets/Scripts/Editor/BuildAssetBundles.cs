﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEditor;
using UnityEngine;

public class BuildAssetBundles
{
	[MenuItem("Assets/Export Assets")]
	private static void ExportAssets()
	{
		Directory.CreateDirectory("GameBundles");
		BuildPipeline.BuildAssetBundles("GameBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
		Directory.CreateDirectory(@"Builds\GameBundles");
		Thread.Sleep(2000);
		foreach (var file in Directory.GetFiles("GameBundles"))
		{
			File.Copy(file, Path.Combine(@"Builds\GameBundles\", Path.GetFileName(file)), true);
		}
	}
}