﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildableDefinition : InspectorDefinition
{
	public static string placementError;
	private Vector3[] seperatedAxis;
	private Vector3[] extents;
	private float[] relations;
	[Range(1.0f, 20.0f)]
	public float maxPlaceDistance = 5.0f;
	[NonSerialized]
	public Bounds bounds;
	[NonSerialized]
	public ConstructionPoint[] points;
	public uint buildableId;

	public override void Load(string name)
	{
		base.Load(name);
		this.seperatedAxis = new Vector3[15];
		this.extents = new Vector3[4];
		this.relations = new float[4];
		this.bounds = this.GetComponent<BoxCollider>().bounds;
		this.points = this.GetComponentsInChildren<ConstructionPoint>(true);
		this.points = this.points.OrderBy<ConstructionPoint, string>((x) => x.debugName).ToArray();
		for (int index = 0; index < this.points.Length; index++)
		{
			this.points[index].constructionId = (uint)index;
		}
	}

	public BaseBuildable CreateBuildable(BuildingPlanner.PlacementInfo info, bool requireValidPlacement = true)
	{
		BaseEntity entity = GameInstance.singleton.CreateEntity(this.prefabName, Vector3.zero, Quaternion.identity);
		bool placed = this.PlaceBuildable(entity, ref info);
		BaseBuildable buildable = entity as BaseBuildable;
		if (requireValidPlacement && !placed)
		{
			if (entity.IsNetworked())
				GameInstance.singleton.DestroyEntity(entity.gameObject, 0.0f);
			return null;
		}
		buildable.definition = this;
		return buildable;
	}

	public static ConstructionPoint GetConstructionPoint(uint target, uint constructionId)
	{
		BaseEntity entity = BaseNetworkable.GetNetworkable(target) as BaseEntity;
		if (entity == null || !entity.enabled)
			return null;
		ConstructionPoint[] points = entity.GetComponentsInChildren<ConstructionPoint>();
		for (int index = 0; index < points.Length; index++)
		{
			ConstructionPoint point = points[index];
			if (point.constructionId == constructionId && point.enabled)
				return point;
		}
		return null;
	}

	public bool PlaceBuildable(BaseEntity buildable, ref BuildingPlanner.PlacementInfo info)
	{
		if (!info.isValid || buildable == null)
			return false;
		buildable.transform.position = info.position;
		buildable.transform.rotation = info.rotation;
		buildable.OnTargetTransformUpdate(info.position, info.rotation);
		if (this.IsPlacingInsideCollider(buildable, ref info))
		{
			BuildableDefinition.placementError = "Placing inside entity collider";
			return false;
		}
		if (Vector3.Distance(buildable.transform.position, info.ray.origin) > this.maxPlaceDistance)
		{
			BuildableDefinition.placementError = "Placing outside of range";
			return false;
		}
		BuildableDefinition.placementError = "No error";
		return true;
	}

	public bool IsPlacingInsideCollider(BaseEntity buildable, ref BuildingPlanner.PlacementInfo info)
	{
		foreach (Collider collider in MonoBehaviour.FindObjectsOfType<Collider>())
		{
			if (collider.GetComponent<BaseEntity>() != null && collider != buildable.GetComponent<Collider>() && this.CheckIntersect(info.position, info.rotation, collider))
				return true;
		}
		return false;
	}

	private bool CheckIntersect(Vector3 position, Quaternion rotation, Collider collider)
	{
		Bounds bounds = collider.GetComponent<BaseEntity>().GetAxisAlignedBounds();
		Vector3 diff = bounds.center - position;
		this.seperatedAxis[0] = collider.transform.forward;
		this.seperatedAxis[1] = collider.transform.right;
		this.seperatedAxis[2] = collider.transform.up;
		this.seperatedAxis[3] = rotation * Vector3.forward;
		this.seperatedAxis[4] = rotation * Vector3.right;
		this.seperatedAxis[5] = rotation * Vector3.up;
		this.seperatedAxis[6] = Vector3.Cross(this.seperatedAxis[0], this.seperatedAxis[3]).normalized;
		this.seperatedAxis[7] = Vector3.Cross(this.seperatedAxis[0], this.seperatedAxis[4]).normalized;
		this.seperatedAxis[8] = Vector3.Cross(this.seperatedAxis[0], this.seperatedAxis[5]).normalized;
		this.seperatedAxis[9] = Vector3.Cross(this.seperatedAxis[1], this.seperatedAxis[3]).normalized;
		this.seperatedAxis[10] = Vector3.Cross(this.seperatedAxis[1], this.seperatedAxis[4]).normalized;
		this.seperatedAxis[11] = Vector3.Cross(this.seperatedAxis[1], this.seperatedAxis[5]).normalized;
		this.seperatedAxis[12] = Vector3.Cross(this.seperatedAxis[2], this.seperatedAxis[3]).normalized;
		this.seperatedAxis[13] = Vector3.Cross(this.seperatedAxis[2], this.seperatedAxis[4]).normalized;
		this.seperatedAxis[14] = Vector3.Cross(this.seperatedAxis[2], this.seperatedAxis[5]).normalized;
		this.extents[0] = this.seperatedAxis[0] * bounds.size.z + this.seperatedAxis[1] * bounds.size.x + this.seperatedAxis[2] * bounds.size.y;
		this.extents[1] = -this.seperatedAxis[0] * bounds.size.z + this.seperatedAxis[1] * bounds.size.x + this.seperatedAxis[2] * bounds.size.y;
		this.extents[2] = this.seperatedAxis[3] * this.bounds.size.z + this.seperatedAxis[4] * this.bounds.size.x + this.seperatedAxis[5] * this.bounds.size.y;
		this.extents[3] = -this.seperatedAxis[3] * this.bounds.size.z + this.seperatedAxis[4] * this.bounds.size.x + this.seperatedAxis[5] * this.bounds.size.y;
		float R = 0.0f;
		for (int index = 0; index < this.seperatedAxis.Length; index++)
		{
			if (this.seperatedAxis[index] != Vector3.zero)
			{
				R = Vector3.Dot(diff, this.seperatedAxis[index]);
				this.relations[0] = Vector3.Dot(this.extents[0], this.seperatedAxis[index]);
				this.relations[1] = Vector3.Dot(this.extents[1], this.seperatedAxis[index]);
				this.relations[2] = Vector3.Dot(this.extents[2], this.seperatedAxis[index]);
				this.relations[3] = Vector3.Dot(this.extents[3], this.seperatedAxis[index]);
				if (Mathf.Abs(R) > Mathf.Abs(this.relations[0]) + Mathf.Abs(this.relations[2]) || Mathf.Abs(R) > Mathf.Abs(this.relations[1]) + Mathf.Abs(this.relations[3]))
				{
					return false;
				}
			}
		}
		return true;
	}
}