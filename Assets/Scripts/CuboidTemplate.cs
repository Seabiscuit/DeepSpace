﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CuboidTemplate : GeometricEditor.BaseTemplate
{
	public override void InitAttributes()
	{
		ScaleAttribute scale = new ScaleAttribute();
		scale.effector.scale = 1.0f;
		base.attributes.Add(scale);
	}
}