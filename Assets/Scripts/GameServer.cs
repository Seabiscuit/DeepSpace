﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using UnityEngine;

public class GameServer : MonoBehaviour, IServerCallback
{
	private List<BasePlayer> players = new List<BasePlayer>();
	private Dictionary<Message.Type, Action<Message, Connection>> handlers = new Dictionary<Message.Type, Action<Message, Connection>>();
	public ushort serverId = 1;
	[NonSerialized]
	public string localIpAddress = "localhost";
	private HashSet<ulong> bannedPlayers = new HashSet<ulong>();
	protected Server server = new Server();
	public const int maxConnections = 10;
	public bool isActive;

	public void Initialize(string loadFrom = "")
	{
		MethodInfo[] methods = typeof(GameServer).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic);
		foreach (MethodInfo method in methods)
		{
			object attribute = method.GetCustomAttributes(typeof(Message.Handler), false).FirstOrDefault();
			if (attribute == null)
				continue;
			Message.Handler descriptor = attribute as Message.Handler;
			Action<Message, Connection> action = Delegate.CreateDelegate(typeof(Action<Message, Connection>), this, method) as Action<Message, Connection>;
			this.handlers.Add(descriptor.type, action);
		}
		BaseEntity.RegisterRpcTargets();
		this.server.Init();
		this.OpenNetwork();
	}

	public void OpenNetwork()
	{
		if (this.isActive)
			return;
		if (!this.server.Start(maxConnections, this.serverId))
			Debug.LogError("Server start failed");
		else
		{
			this.server.callback = this;
			this.isActive = true;
		}
	}

	public void CloseNetwork()
	{
		if (!this.isActive)
			return;
		if (!this.server.Stop(true, "Connection closed"))
			Debug.LogError("Server stop failed");
		else
			this.isActive = false;
	}

	private void OnEnable()
	{
		Application.targetFrameRate = 120;
		this.Initialize();
	}

	private void OnApplicationQuit()
	{
		this.CloseNetwork();
	}

	public void OnPeerAuth(Connection connection)
	{
	}

	public void OnPeerDisconnected(Connection connection, string reason)
	{
		this.KillPlayer(connection);
		List<BaseNetworkable> networkables = BaseNetworkable.GetNetworkables();
		if (networkables != null)
		{
			foreach (BaseNetworkable networkable in networkables)
			{
				if (networkable.IsSubscriber(connection))
					networkable.RemoveSubscriber(connection);
			}
		}
	}

	public void OnMessageReceived(Message message, Connection sender)
	{
		Action<Message, Connection> action;
		if (this.handlers.TryGetValue(message.type, out action))
			action.Invoke(message, sender);
	}

	private void Update()
	{
		if (this.server != null && this.server.isActive)
			this.server.Update();
	}

	public static Vector3 GetSpawnPoint()
	{
		return new Vector3(UnityEngine.Random.Range(-5f, 5f) + -30000.0f, 0f, -90000.0f);
	}

	[Message.Handler(Message.Type.Auth)]
	private void PeerAuth(Message message, Connection connection)
	{
		ulong networkId = message.buffer.ReadUlong();
		if (this.bannedPlayers.Contains(networkId))
			return;
		string displayName = "Debug";
		bool hasAuth;
		if (!connection.HasDisplayName())
			connection.displayName = displayName;
		Message response = new Message();
		response.buffer.WriteByte((byte)Message.Type.Auth);
		if (this.server.GetConnection(networkId) != null || connection.isAuthenicated)
			hasAuth = false;
		else
		{
			this.server.OnPeerAuth(connection, networkId);
			hasAuth = true;
		}
		response.buffer.WriteBool(hasAuth);
		this.server.SendMessage(response, connection, true);
		if (!hasAuth)
		{
			connection.Close();
		}
	}

	[Message.Handler(Message.Type.Ready)]
	private void ClientReady(Message message, Connection connection)
	{
		if (this.server == null || !connection.isAuthenicated)
			return;
		connection.IsReady = true;
		List<BaseNetworkable> networkables = BaseNetworkable.GetNetworkables();
		foreach (BaseNetworkable networkable in networkables)
			networkable.AddSubscriber(connection);
		this.SpawnNewPlayer(connection);
	}

	private void SpawnNewPlayer(Connection connection)
	{
		Vector3 spawnPoint = GameServer.GetSpawnPoint();
		BasePlayer player = GameInstance.singleton.CreateEntity("assets/prefabs/entity/player.prefab", spawnPoint, Quaternion.identity) as BasePlayer;
		player.connection = connection;
		player.displayName = connection.displayName;
		if (connection.player != null)
			Debug.LogError("Spawning a new player for a connection that already has an active instance");
		connection.player = player;
		this.players.Add(player);
		player.SubscribeAll();
	}

	private void KillPlayer(Connection connection)
	{
		BasePlayer player = connection.player;
		if (player != null)
		{
			GameInstance.singleton.DestroyEntity(player.gameObject);
			connection.player = null;
		}
	}

	[Message.Handler(Message.Type.Update)]
	private void PlayerUpdate(Message message, Connection connection)
	{
		if (!connection.HasPlayer || !connection.isAuthenicated)
			return;
		BasePlayer player = connection.player;
		Network.PlayerUpdate update = new Network.PlayerUpdate();
		update.FromMessage(message);
		Vector3 position = update.bodyPosition;
		if (position.IsNaNOrInfinity())
			this.server.Kick(connection.networkId, "Invalid position");
		else
			player.OnTargetTransformUpdate(position, update.bodyRotation);
		List<Connection> subscribers = player.GetSubscribers();
		if (subscribers != null)
		{
			if (subscribers.Count < 1)
				return;
			Message response = new Message();
			response.buffer.WriteType(Message.Type.Update);
			update.ToMessage(response); // Optimise this asap
			response.buffer.WriteUint(player.networkableId);
			this.server.SendMessage(response, subscribers, false);
		}
	}

	[Message.Handler(Message.Type.Rpc)]
	private void PlayerRpcMessage(Message message, Connection connection)
	{
		uint networkableId = message.buffer.ReadUint();
		if (!connection.isAuthenicated || networkableId == 0U)
			return;
		uint rpcId = message.buffer.ReadUint();
		MethodInfo rpc;
		if (BaseEntity.rpcTargets.TryGetValue(rpcId, out rpc))
		{
			BaseEntity entity = BaseNetworkable.GetNetworkable(networkableId) as BaseEntity;
			if (entity != null)
			{
				rpc.Invoke(entity, new object[] { null, message });
			}
		}
	}

	[Message.Handler(Message.Type.Close)]
	private void PlayerDisconnect(Message message, Connection connection)
	{
		string reason = message.buffer.ReadString();
		if (connection.isAuthenicated)
			connection.isAuthenicated = false;
		connection.Close();
	}
}