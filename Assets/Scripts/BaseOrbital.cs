﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseOrbital : BaseEntity
{
	public double radius;
	public double density;
	public double mass;
	public double gravParameter;

	public double GetDiameter()
	{
		return this.radius * 2;
	}

	public double GetVolume()
	{
		return this.radius * this.radius * this.radius * Math.PI * 1.333333;
	}

	protected override void OnNetworkInit()
	{
		this.radius = 25000;
		this.density = 5219;
		double v = this.GetVolume();
		this.mass = this.density * v;
		this.gravParameter = this.mass * 6.67408E-11;
	}
}